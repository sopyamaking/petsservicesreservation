<?php

require_once "setup.php";

use Slim\Views\Twig;
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require_once "account.php";
require_once "admin.php";
require_once "doctor.php";


const PRODUCTS_PER_PAGE = 3;



$app->get('/session', function (Request $request, Response $response, array $args) {
    $body = "<pre>\n\$_SESSION:\n" . print_r($_SESSION, true);
    $response->getBody()->write($body);
    return $response;
});

$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.html.twig');
});

$app->get('/team', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $doctorsList = DB::query("SELECT d.id, d.name , d.description,d.photoFilePath,s.name AS serviceName 
                                 FROM doctors AS d
                                 JOIN services AS s
                                 ON d.serviceId = s.id 
                                    ORDER BY id"); 
    return $view->render($response, 'team.html.twig',[ 'doctorsList'=>$doctorsList]);
});

$app->get('/services', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $generalDescriptionList = DB::query("SELECT s.id, s.description 
                                 FROM services AS s                                  
                                 WHERE s.categoryId = 2                                                                 
                                 ORDER BY id"); 
    $surgeryDescriptionList = DB::query("SELECT s.id, s.description 
                                        FROM services AS s                                  
                                        WHERE s.categoryId = 5                                                                 
                                        ORDER BY id");
    $microchippingDescriptionList = DB::query("SELECT s.id, s.description   
                                                FROM services AS s                                  
                                                WHERE s.categoryId = 4                                                                 
                                                ORDER BY id");
    $otherDescriptionList = DB::query("SELECT s.id, s.description   
                                        FROM services AS s                                  
                                        WHERE s.categoryId = 6                                                                 
                                        ORDER BY id");
    $radiographieDescriptionList = DB::query("SELECT s.id, s.description   
                                                FROM services AS s                                  
                                                WHERE s.categoryId = 3                                                                 
                                                ORDER BY id");
    $preventionDescriptionList = DB::query("SELECT s.id, s.description   
                                            FROM services AS s                                  
                                            WHERE s.categoryId = 1                                                                 
                                            ORDER BY id");
    return $view->render($response, 'services.html.twig',[ 'generalDescriptionList'=>$generalDescriptionList,'surgeryDescriptionList'=>$surgeryDescriptionList,
    'microchippingDescriptionList'=>$microchippingDescriptionList,'otherDescriptionList'=>$otherDescriptionList,'radiographieDescriptionList'=>$radiographieDescriptionList,'preventionDescriptionList'=>$preventionDescriptionList]);
});

$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/contact', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'contact.html.twig');
});

$app->get('/appointment', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) ) {
        return $response->withHeader('Location', '/login')->withStatus(302);        
    }    
    $petId = $_SESSION['user']['id'];
    $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate,av.startTime,av.endTime,a.description,a.status,p.petName,p.petType,p.age AS petAge,s.name AS serviceName,d.name AS doctorName
                                    FROM appointments AS a 
                                    join availibilities  as av on a.availibilityId = av.id 
                                    Join pets AS p ON a.petId = p.id
                                    JOIN services AS s ON a.serverId = s.id
                                    JOIN doctors AS d ON a.doctorId = d.id
                                    WHERE petId = :i",$petId);
    
    $datesList = [date('Y-m-d ',strtotime('+1 day')),date('Y-m-d ',strtotime('+2 day')),
                    date('Y-m-d ',strtotime('+3 day')),date('Y-m-d ',strtotime('+4 day')),
                    date('Y-m-d ',strtotime('+5 day')),date('Y-m-d ',strtotime('+6 day')),date('Y-m-d ',strtotime('+7 day'))];  
    $servicesList = DB::query("SELECT * FROM services ");
    $doctorsList = DB::query("SELECT * FROM doctors ");
    $timesList = ['09:00:00-10:00:00','10:00:00-11:00:00','11:00:00-12:00:00','12:00:00-13:00:00','13:00:00-14:00:00','14:00:00-15:00:00','15:00:00-16:00:00','16:00:00-17:00:00'];
    return $view->render($response, 'appointment.html.twig',['appointmentsList'=>$appointmentsList,'datesList'=>$datesList,'servicesList'=>$servicesList,'doctorsList'=>$doctorsList,'timesList'=>$timesList]);
});

$app->get('/ajax/loaddoctors[/by/{id}]', function (Request $request, Response $response, array $args) {
    $id = isset($args['id']) ? $args['id'] : "";   
    $doctors=null;    
    if($id)
    {
        $doctors = DB::query("SELECT * FROM doctors WHERE serviceId = :i",$id);
    }
    else{
        $doctors = DB::query("SELECT * FROM doctors ");
     }    
    if(!$doctors){
        echo "";
    }else{
        foreach ($doctors as $row) {         
            echo '<option value='.$row['id'].' >'.$row['name'].'</option>';          
        }
    }
    return $response;
});
 
$app->post('/ajax/loadtimes', function (Request $request, Response $response, array $args) {
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    global $log;
    $json = $request->getBody();
    $data = json_decode($json, true);
    $id = $data['doctorid'];
    $date = $data['date'];
 
    $times=null;    
    if($id&&$date)
    {
        $times = DB::query("SELECT av.id,av.startTime,av.endTime
                             FROM `availibilities` av 
                             left join appointments ap on av.id=ap.availibilityId 
                            WHERE ap.id is null and av.doctorId=:i and av.availableDate=:s",$id,$date);
        $strHtml = "";
        foreach ($times as $row) {    
            $strHtml .= '<option value='.$row['id'].' >'.$row['startTime'].'-'.$row['endTime'].'</option>';          
        }
        $response = $response->withStatus(201); // record created
        $response->getBody()->write(json_encode($strHtml));
    }
    else{
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - data error!"));
        return $response;
     }
    return $response;
});

$app->post('/appointment', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request); 
    $timesList = ['09:00:00-10:00:00','10:00:00-11:00:00','11:00:00-12:00:00','12:00:00-13:00:00','13:00:00-14:00:00','14:00:00-15:00:00','15:00:00-16:00:00','16:00:00-17:00:00'];
    $datesList = [date('Y-m-d ',strtotime('+1 day')),date('Y-m-d ',strtotime('+2 day')),
                    date('Y-m-d ',strtotime('+3 day')),date('Y-m-d ',strtotime('+4 day')),date('Y-m-d ',strtotime('+5 day')),date('Y-m-d ',strtotime('+6 day')),date('Y-m-d ',strtotime('+7 day'))];     
    $servicesList = DB::query("SELECT * FROM services ");
                    $errorsList = array();
    $doctorsList = DB::query("SELECT * FROM doctors ");
    $petId = $_SESSION['user']['id'];
   $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate, av.startTime,av.endTime,a.description,a.status,p.petName,p.petType,p.age AS petAge,s.name AS serviceName,d.name AS doctorName
                                    FROM appointments AS a
                                    join availibilities  as av on a.availibilityId = av.id
                                    Join pets AS p ON a.petId = p.id
                                    JOIN services AS s ON a.serverId = s.id
                                    JOIN doctors AS d ON a.doctorId = d.id
                                    WHERE petId = :i",$petId);
     
   
   $postVars = $request->getParsedBody();
   $serviceId = $postVars['service'];   
   $doctorId = $postVars['doctor'];
   $availibilityId = $postVars['time'];    
   $status = $postVars['status'];
   $description = $postVars['description'];      

   //handle the image file  that has been choosen in twig       
    /*if (isNotEmpty($serviceId)==false) {
        array_push($errorsList, "Service must be selected");
        $postVars['service'] = "";
    }
    if (isEmpty($doctorId)) {
        array_push($errorsList, "Doctor must be selected");
        $postVars['doctor'] = "";
    }
    if (isEmpty($availibilityId)) {
        array_push($errorsList, "Time must be selected");
        $postVars['time'] = "";
    }*/
    if (strlen($description) < 2 || strlen($description) > 1000) {
        array_push($errorsList, "Description must be 2-1000 characters long");
        $postVars['description'] = "";
    }   
   
    if($errorsList){
        return $view->render($response, 'appointment.html.twig',['appointmentsList'=>$appointmentsList,'datesList'=>$datesList,'servicesList'=>$servicesList,'doctorsList'=>$doctorsList,'timesList'=>$timesList]);
    }else{
        DB::insert('appointments',['petId'=>$petId,'serverId'=>$serviceId,'doctorId'=>$doctorId,'availibilityId'=>$availibilityId,'status'=>$status,'description'=>$description]);   
        $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate, av.startTime,av.endTime,a.description,a.status,p.petName,p.petType,p.age AS petAge,s.name AS serviceName,d.name AS doctorName
                                        FROM appointments AS a
                                        join availibilities  as av on a.availibilityId = av.id
                                        Join pets AS p ON a.petId = p.id
                                        JOIN services AS s ON a.serverId = s.id
                                        JOIN doctors AS d ON a.doctorId = d.id
                                        WHERE petId = :i",$petId);
    return $view->render($response, 'appointment.html.twig',['appointmentsList'=>$appointmentsList,'datesList'=>$datesList,'servicesList'=>$servicesList,'doctorsList'=>$doctorsList,'timesList'=>$timesList]);
    }      
});

$app->run();