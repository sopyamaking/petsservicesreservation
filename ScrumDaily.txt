2020-5-4 
1. Done since last scrim

2. To do until next Scrum
-Create the database in the PhpMyAdmin
-Do some preparation work for this object ,just like trello ,bitbucket,etc
-Prepare the slim and twig
-Add some data into the database 
3.  Need assistance
-NA

2020-5-5
1. Done since last scrim
-- Create the database in the PhpMyAdmin
-- Create the master.html.twig file
-- Create the index.html.twig file
2. To do until next Scrum
-- Create the register.html.twig file
-- Create the login.html.twig
-- Create the contact.html.twig
-- Create the services.html.twig
3.  Need assistance
-NA

2020-5-6
1. Done since last scrim
-- Create the register.html.twig file and realize the transfer the data between url and database
-- Create the login.html.twig and realize the transfer the data between url and database
-- Create the contact.html.twig
-- Create the services.html.twig
-- Create the logout URL and realize the page function
2. To do until next Scrum
-- Add some data into database
-- Create the index page of doctor
-- Create the index page of admin
3.  Need assistance
-NA

2020-5-7
1. Done since last scrim
-- Add some data into database
2. To do until next Scrum
-- Doctor can login
-- Create the index page of doctor
-- Create the index page of admin
3.  Need assistance
-NA


2020-5-8
1. Done since last scrim
-- doctor can  login
-- admin can login
2. To do until next Scrum
-- Create the index page of doctor
-- Create the index page of admin
3.  Need assistance
-NA

2020-5-9
1. Done since last scrim
-- Doctor can view his/her appointment
-- Admin can manage the appointments in the database
-- Admin can manege the services in the database
2. To do until next Scrum
-- Admin can add the pets information in the database
-- Admin can edit the pets information in the databse
-- Admin can delete the pets information in the database
3.  Need assistance
-NA

2020-5-10
1. Done since last scrim
-- Admin can add the pets information in the database
-- Admin can edit the pets information in the databse
-- Admin can delete the pets information in the database
2. To do until next Scrum
-- Admin can add the doctors information in the database
-- Admin can edit the doctors information in the databse
-- Admin can delete the doctors information in the database
3.  Need assistance
-NA

2020-5-11
1. Done since last scrim
-- Admin can add the doctors information in the database
-- Admin can delete the doctors information in the database
2. To do until next Scrum
-- Modify the service page
-- Admin can edit the doctors information in the databse
3.  Need assistance
-NA

2020-5-12
1. Done since last scrim
-- Admin can edit the doctors information in the databse
-- Modify the service page
-- Modify the index template
2. To do until next Scrum
-- create the team page
-- create the appointment template
-- Admin can manage the categories
3.  Need assistance
-NA

2020-5-13
1. Done since last scrim
-- create the team page
-- create the appointment template
-- user can choose a date of an appointment
-- user can choose a service of an appointment
2. To do until next Scrum
-- user can choose an available doctor 
-- add some data of availibilities table into the database
-- Admin can manage the categories
3.  Need assistance
-NA

2020-5-14
1. Done since last scrim
-- add some data into the availibilities table
-- doctor can view all his availibilities in 7 days
2. To do until next Scrum
-- doctor can add availibilities
-- user can choose the doctor of a service 
-- user can choose the available time of the selected  doctor
3.  Need assistance
-NA


2020-5-15
1. Done since last scrim
-- doctor can add availibilities
-- testing some page
2. To do until next Scrum
-- user can choose the doctor of a service 
-- user can choose the available time of the selected  doctor
3.  Need assistance
-NA

2020-5-16
1. Done since last scrim
-- user can choose the doctor of a service 
-- user can choose the available time of the selected  doctor
-- user can make an appointment successful
2. To do until next Scrum
-- modify some template 
-- testing 
3.  Need assistance
-NA