<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin_products_list.html.twig */
class __TwigTemplate_89cf7d5fc9dc2e3059bbb7f8cc205aa9bbd87d715eb31e6a873fdca6a7c04bbb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "admin_products_list.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Products list";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <p><a href=\"/admin/products/add\">add product</a></p>
    <table border=\"1\">
        <tr><th>#</th><th>product name</th><th>category name</th><th>description</th><th>price</th><th>image</th><th>actions</th></tr>
        ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["productsList"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 10
            echo "        <tr>
            <td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "productId", [], "any", false, false, false, 11), "html", null, true);
            echo "</td>
            <td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "productName", [], "any", false, false, false, 12), "html", null, true);
            echo "</td>
            <td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "categoryName", [], "any", false, false, false, 13), "html", null, true);
            echo "</td>
            <td>";
            // line 14
            echo twig_get_attribute($this->env, $this->source, $context["p"], "description", [], "any", false, false, false, 14);
            echo "</td>
            <td>";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "unitPrice", [], "any", false, false, false, 15), "html", null, true);
            echo "</td>
            <td><img src=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "pictureFilePath", [], "any", false, false, false, 16), "html", null, true);
            echo "\" width=\"150\"></td>
            <td>
                <!-- method 1 - simple h-ref text link -->
                <!-- <a href=\"/admin/products/edit/";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "id", [], "any", false, false, false, 19), "html", null, true);
            echo "\">Edit</a> -->
                <!-- method 2 - button with javascript -->
                <button onclick=\"window.location='/admin/products/delete/";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "productId", [], "any", false, false, false, 21), "html", null, true);
            echo "';\">Delete</button>
                <!-- method 3 - form with submit button -->
                <button onclick=\"window.location='/admin/products/edit/";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "productId", [], "any", false, false, false, 23), "html", null, true);
            echo "';\">Edit</button>
                <!--<form action=\"/admin/products/edit/";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["p"], "id", [], "any", false, false, false, 24), "html", null, true);
            echo "\">
                    <input type=\"submit\" value=\"Edit\">
                </form>-->
            </td>
        </tr>                
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    </table>
";
    }

    public function getTemplateName()
    {
        return "admin_products_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 30,  110 => 24,  106 => 23,  101 => 21,  96 => 19,  90 => 16,  86 => 15,  82 => 14,  78 => 13,  74 => 12,  70 => 11,  67 => 10,  63 => 9,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Products list{% endblock %}

{% block content %}
    <p><a href=\"/admin/products/add\">add product</a></p>
    <table border=\"1\">
        <tr><th>#</th><th>product name</th><th>category name</th><th>description</th><th>price</th><th>image</th><th>actions</th></tr>
        {% for p in productsList %}
        <tr>
            <td>{{p.productId}}</td>
            <td>{{p.productName}}</td>
            <td>{{p.categoryName}}</td>
            <td>{{p.description  | raw }}</td>
            <td>{{p.unitPrice}}</td>
            <td><img src=\"{{p.pictureFilePath}}\" width=\"150\"></td>
            <td>
                <!-- method 1 - simple h-ref text link -->
                <!-- <a href=\"/admin/products/edit/{{p.id}}\">Edit</a> -->
                <!-- method 2 - button with javascript -->
                <button onclick=\"window.location='/admin/products/delete/{{p.productId}}';\">Delete</button>
                <!-- method 3 - form with submit button -->
                <button onclick=\"window.location='/admin/products/edit/{{p.productId}}';\">Edit</button>
                <!--<form action=\"/admin/products/edit/{{p.id}}\">
                    <input type=\"submit\" value=\"Edit\">
                </form>-->
            </td>
        </tr>                
        {% endfor %}
    </table>
{% endblock content %}", "admin_products_list.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\admin_products_list.html.twig");
    }
}
