<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin_products_delete_success.html.twig */
class __TwigTemplate_d0abb2af9dac044fdd4774a77308c955a53b9a7acde576764e7752f598c60f53 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "admin_products_delete_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product delete ";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <p>Product id : ";
        echo twig_escape_filter($this->env, ($context["productId"] ?? null), "html", null, true);
        echo " has been deleted successful! </p>
    <p><a href=\"/admin/products/list\">Click here to Home</a></p>
";
    }

    public function getTemplateName()
    {
        return "admin_products_delete_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Product delete {% endblock %}

{% block content %}
    <p>Product id : {{productId}} has been deleted successful! </p>
    <p><a href=\"/admin/products/list\">Click here to Home</a></p>
{% endblock %}", "admin_products_delete_success.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\admin_products_delete_success.html.twig");
    }
}
