<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* cart_add.html.twig */
class __TwigTemplate_d8ce6e7ef4a46c539d80d709c05d09adb7fa6a388524b4ca8347dd2d975816c8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "cart_add.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "cart add ";
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "
    <h3>This item has added in your cart successfully!</h3>Please <a href=\"http://day06eshop.ipd20:8888/\">continue shopping</a><br/>
    <hr />
    <div class=\"container\"> 
            ";
        // line 9
        if (($context["items"] ?? null)) {
            // line 10
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 11
                echo "                 <div class=\"items\">
                    <div class=\"prodPicture\">
                        <img src=\"";
                // line 13
                echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["item"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["pictureFilePath"] ?? null) : null), "html", null, true);
                echo "\" width=\"100\" height=\"100\">
                    </div>
                    <div class=\"prodContent\">
                        <h5>";
                // line 16
                echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["item"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["name"] ?? null) : null), "html", null, true);
                echo "</h5>  
                        <p style=\"font-weight:bold;color:red\">\$";
                // line 17
                echo twig_escape_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["item"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["unitPrice"] ?? null) : null), "html", null, true);
                echo "</p>     
                        <p>";
                // line 18
                echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["item"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["quantity"] ?? null) : null), "html", null, true);
                echo "</p>                 
                        <p><button class=\"btn btn-success\">Add to cart</button><button class=\"btn btn-success\">Delete</button></p>
                    </div>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "
            ";
        }
        // line 24
        echo "    
        </div>


";
    }

    public function getTemplateName()
    {
        return "cart_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 24,  100 => 23,  89 => 18,  85 => 17,  81 => 16,  75 => 13,  71 => 11,  66 => 10,  64 => 9,  58 => 5,  54 => 4,  47 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}
{% block title %}cart add {% endblock %}

{% block content %}

    <h3>This item has added in your cart successfully!</h3>Please <a href=\"http://day06eshop.ipd20:8888/\">continue shopping</a><br/>
    <hr />
    <div class=\"container\"> 
            {% if items %}
                {% for item in items %}
                 <div class=\"items\">
                    <div class=\"prodPicture\">
                        <img src=\"{{item['pictureFilePath']}}\" width=\"100\" height=\"100\">
                    </div>
                    <div class=\"prodContent\">
                        <h5>{{ item['name'] }}</h5>  
                        <p style=\"font-weight:bold;color:red\">\${{ item['unitPrice'] }}</p>     
                        <p>{{ item['quantity']}}</p>                 
                        <p><button class=\"btn btn-success\">Add to cart</button><button class=\"btn btn-success\">Delete</button></p>
                    </div>
                </div>
                {% endfor %}

            {% endif %}    
        </div>


{% endblock %}", "cart_add.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\cart_add.html.twig");
    }
}
