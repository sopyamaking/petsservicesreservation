<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contact.html.twig */
class __TwigTemplate_1f0a0d80617d150b10288e1c404450b168c38f5be72b2a3f00c180df68bf76d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "contact.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Contact us";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo " <div id=\"contactContainer\" class=\"container\">\t
    <div class=\"row  justify-content-center\">
        <div class=\"col-6\">
            <img src=\"../images/layout/attempt5.png\" alt=\"dogPic\"  width=\"300\"
\t\t\t\t\t\t height=\"100\"class=\"w-100\"/>
        </div>
    </div><br>    
\t
\t<!-- Wrapper of blue and white column -->
\t<div class=\"row mx-0 w-100 h-100\">
\t\t<!-- White column -->
       
\t\t<div class=\"col-12 col-md-6 d-md-flex align-items-md-center bg-light\">       
\t\t\t<!-- Contact Us -->
           
\t\t\t<div class=\"row p-2 pb-4\">
\t\t\t\t<div class=\"col-12 mb-3\">                
\t\t\t\t\t<h2>Contact Us</h2>
\t\t\t\t\t<p>genericEmail@hotmail.com</p>
\t\t\t\t\t<p>T:555-555-5555</p>
                    <p>F:514-514-515</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12\"> 
\t\t\t\t\t<a href=\"/reservation\" class=\"button d-inline-block mb-1 mb-md-2 px-2 py-1 border border-secondary rounded-lg text-center text-secondary text-decoration-none\" >Make an Appointment</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12 mb-3\">
\t\t\t\t\t<a href=\"/services\" class=\"button d-inline-block px-2 py-1 border border-secondary rounded-lg text-center text-secondary text-decoration-none\" >Our Services</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t<!-- End of Contact Us -->

\t\t</div>
\t\t<!-- End of white column -->

\t\t<!-- Blue column -->
\t\t<div class=\"col-12 col-md-6 p-3 d-md-flex align-items-md-center text-white bg-secondary\">
\t\t\t<!-- Opening hours -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 col-xl-8 p-3\">
\t\t\t\t\t<h3>Opening hours</h3>

\t\t\t\t\t<ul class=\"p-1 mb-0\">
\t\t\t\t\t\t<li class=\"mb-2 d-flex justify-content-between border-bottom small\">
\t\t\t\t\t\t\t<p>Monday to Thursday</p>
\t\t\t\t\t\t\t<p>from 6:00 a.m. to 6:00 p.m.</p>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"mb-2 d-flex justify-content-between border-bottom small\">
\t\t\t\t\t\t\t<p>Friday</p>
\t\t\t\t\t\t\t<p>from 6:00 a.m. to 4:00 p.m.</p>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"d-flex justify-content-between border-bottom small\">
\t\t\t\t\t\t\t<p>Saturday</p>
\t\t\t\t\t\t\t<p>from 9:00 a.m. to 12:00 p.m.</p>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<!-- End of Opening hours -->

\t\t\t\t<!-- Reservation info -->
\t\t\t\t<div class=\"col-12 col-xl-8 p-3\">
\t\t\t\t\t<h2>Reservation information</h2>
\t\t\t\t\t<p class=\"text-justify small\"> If there is no availability on reservation for the date you have selected,
                     do not hesitate to contact the office by phone. We may be able to add your name to the waiting list or 
                     have received a last minute cancellation.
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<!-- End of Reservation info -->
\t\t\t</div>
\t\t</div>
\t\t<!-- End of Blue column -->

\t</div>
\t<!-- End of Wrapper of blue and white column -->
</div>
<br><br>

<!-- ****End of contactContainer**** -->

";
    }

    public function getTemplateName()
    {
        return "contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Contact us{% endblock %}

{% block content %}
 <div id=\"contactContainer\" class=\"container\">\t
    <div class=\"row  justify-content-center\">
        <div class=\"col-6\">
            <img src=\"../images/layout/attempt5.png\" alt=\"dogPic\"  width=\"300\"
\t\t\t\t\t\t height=\"100\"class=\"w-100\"/>
        </div>
    </div><br>    
\t
\t<!-- Wrapper of blue and white column -->
\t<div class=\"row mx-0 w-100 h-100\">
\t\t<!-- White column -->
       
\t\t<div class=\"col-12 col-md-6 d-md-flex align-items-md-center bg-light\">       
\t\t\t<!-- Contact Us -->
           
\t\t\t<div class=\"row p-2 pb-4\">
\t\t\t\t<div class=\"col-12 mb-3\">                
\t\t\t\t\t<h2>Contact Us</h2>
\t\t\t\t\t<p>genericEmail@hotmail.com</p>
\t\t\t\t\t<p>T:555-555-5555</p>
                    <p>F:514-514-515</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12\"> 
\t\t\t\t\t<a href=\"/reservation\" class=\"button d-inline-block mb-1 mb-md-2 px-2 py-1 border border-secondary rounded-lg text-center text-secondary text-decoration-none\" >Make an Appointment</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-12 mb-3\">
\t\t\t\t\t<a href=\"/services\" class=\"button d-inline-block px-2 py-1 border border-secondary rounded-lg text-center text-secondary text-decoration-none\" >Our Services</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t<!-- End of Contact Us -->

\t\t</div>
\t\t<!-- End of white column -->

\t\t<!-- Blue column -->
\t\t<div class=\"col-12 col-md-6 p-3 d-md-flex align-items-md-center text-white bg-secondary\">
\t\t\t<!-- Opening hours -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-12 col-xl-8 p-3\">
\t\t\t\t\t<h3>Opening hours</h3>

\t\t\t\t\t<ul class=\"p-1 mb-0\">
\t\t\t\t\t\t<li class=\"mb-2 d-flex justify-content-between border-bottom small\">
\t\t\t\t\t\t\t<p>Monday to Thursday</p>
\t\t\t\t\t\t\t<p>from 6:00 a.m. to 6:00 p.m.</p>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"mb-2 d-flex justify-content-between border-bottom small\">
\t\t\t\t\t\t\t<p>Friday</p>
\t\t\t\t\t\t\t<p>from 6:00 a.m. to 4:00 p.m.</p>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"d-flex justify-content-between border-bottom small\">
\t\t\t\t\t\t\t<p>Saturday</p>
\t\t\t\t\t\t\t<p>from 9:00 a.m. to 12:00 p.m.</p>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t<!-- End of Opening hours -->

\t\t\t\t<!-- Reservation info -->
\t\t\t\t<div class=\"col-12 col-xl-8 p-3\">
\t\t\t\t\t<h2>Reservation information</h2>
\t\t\t\t\t<p class=\"text-justify small\"> If there is no availability on reservation for the date you have selected,
                     do not hesitate to contact the office by phone. We may be able to add your name to the waiting list or 
                     have received a last minute cancellation.
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t\t<!-- End of Reservation info -->
\t\t\t</div>
\t\t</div>
\t\t<!-- End of Blue column -->

\t</div>
\t<!-- End of Wrapper of blue and white column -->
</div>
<br><br>

<!-- ****End of contactContainer**** -->

{% endblock %}", "contact.html.twig", "C:\\xampp\\htdocs\\ipd20\\petserver\\templates\\contact.html.twig");
    }
}
