<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* login.html.twig */
class __TwigTemplate_a15ff99f39d6faf38ff906760e20d997c0e354a56c372b37fc4d00078806c060 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Login";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <div id=\"loginContainer\" class=\"container\">
        <div class=\"row  justify-content-center\">
            <div class=\"col-6\">
                <img src=\"../images/layout/attempt5.png\" alt=\"dogPic\"  width=\"300\"
\t\t\t\t\t\t height=\"100\"class=\"w-100\"/>
            </div>
        </div><br>
        <div class=\"container\">
           ";
        // line 14
        if (($context["error"] ?? null)) {
            // line 15
            echo "                <p class=\"errorMessage\">Login failed. You may try again.</p>
            ";
        }
        // line 17
        echo "        </div>
              
<!-- Form for login-->
    <form id=\"loginForm\" class=\"mt-3 needs-validation\" method=\"post\" >

        <!-- UserName input -->       
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"username\">User name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"username\" id=\"username\" placeholder=\"Your User Name\"  required/>
            </div>
        </div>
        <!-- End of UserName input -->       

        <!-- password input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"password\">Password</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"Your Password\"  required/>
            </div>
        </div>
        <!-- End of password input --> 

        <!-- Submit button -->
        <div class=\"row \">
            <div class=\"col-12\">
                <button type=\"submit\" class=\"btn btn-secondary w-100\">Sign up</button>
            </div>            
        </div>
    </form> 

    <div class=\"row\">
        <div class=\"col-12\">
            <img src=\"./images/staff/dog.png\" alt=\"dogPic\" class=\"w-100\"/>
        </div>
    </div>

 </div>
<!-- End of Container -->    

";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 17,  70 => 15,  68 => 14,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Login{% endblock %}

{% block content %}
    <div id=\"loginContainer\" class=\"container\">
        <div class=\"row  justify-content-center\">
            <div class=\"col-6\">
                <img src=\"../images/layout/attempt5.png\" alt=\"dogPic\"  width=\"300\"
\t\t\t\t\t\t height=\"100\"class=\"w-100\"/>
            </div>
        </div><br>
        <div class=\"container\">
           {% if error %}
                <p class=\"errorMessage\">Login failed. You may try again.</p>
            {% endif %}
        </div>
              
<!-- Form for login-->
    <form id=\"loginForm\" class=\"mt-3 needs-validation\" method=\"post\" >

        <!-- UserName input -->       
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"username\">User name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"username\" id=\"username\" placeholder=\"Your User Name\"  required/>
            </div>
        </div>
        <!-- End of UserName input -->       

        <!-- password input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"password\">Password</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"Your Password\"  required/>
            </div>
        </div>
        <!-- End of password input --> 

        <!-- Submit button -->
        <div class=\"row \">
            <div class=\"col-12\">
                <button type=\"submit\" class=\"btn btn-secondary w-100\">Sign up</button>
            </div>            
        </div>
    </form> 

    <div class=\"row\">
        <div class=\"col-12\">
            <img src=\"./images/staff/dog.png\" alt=\"dogPic\" class=\"w-100\"/>
        </div>
    </div>

 </div>
<!-- End of Container -->    

{% endblock %}", "login.html.twig", "C:\\xampp\\htdocs\\ipd20\\petserver\\templates\\login.html.twig");
    }
}
