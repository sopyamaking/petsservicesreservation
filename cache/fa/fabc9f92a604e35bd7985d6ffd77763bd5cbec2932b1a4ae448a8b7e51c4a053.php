<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* error_forbidden.html.twig */
class __TwigTemplate_20b7e00fcb3e17d9f503e7b1acc3fc3753ed76ec66e419d1dd5dcc1ac10eaa20 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "error_forbidden.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Access denied";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<p><h3>You can not access !</h3></p>

<p><a href=\"/\">Click here</a> to continue.</p>

<img src=\"/images/forbidden.png\" width=\"200\">

";
    }

    public function getTemplateName()
    {
        return "error_forbidden.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Access denied{% endblock %}

{% block content %}

<p><h3>You can not access !</h3></p>

<p><a href=\"/\">Click here</a> to continue.</p>

<img src=\"/images/forbidden.png\" width=\"200\">

{% endblock %}", "error_forbidden.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\error_forbidden.html.twig");
    }
}
