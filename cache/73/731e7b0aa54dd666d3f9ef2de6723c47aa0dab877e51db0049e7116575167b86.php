<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* register.html.twig */
class __TwigTemplate_f582cecf3ba03ccd274661cb60f7600b7965154f844f05f6933c85c3adb88919 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Register";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            //\$('input[name=username]').blur(function() {
            \$('input[name=username]').on('keyup keypress blur change cut paste', function() {
                var username = \$('input[name=username]').val();
                \$('#isTaken').load(\"/register/isusernametaken/\" + username);
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

";
    }

    // line 23
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "
    <div id=\"registerContainer\" class=\"container\">
        <div class=\"row  justify-content-center\">
            <div class=\"col-6\">
                <img src=\"../images/layout/attempt5.png\" alt=\"dogPic\"  width=\"300\"
\t\t\t\t\t\t height=\"100\"class=\"w-100\"/>
            </div>
        </div><br>
        <div class=\"container\">
            ";
        // line 33
        if (($context["errorsArray"] ?? null)) {
            // line 34
            echo "                <ul class=\"errorMessage\">
                    ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsArray"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 36
                echo "                        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                     ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "                </ul>
            ";
        }
        // line 40
        echo "        </div>
<!-- Form for register-->

    <form id=\"resvForm\" class=\"mt-3 needs-validation\" method=\"post\" >

        <!-- UserName input -->
        <div class=\"text-danger\" id=\"isTaken\"></div>
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"username\">User name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"username\" id=\"username\" placeholder=\"Your User Name\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "username", [], "any", false, false, false, 51), "html", null, true);
        echo "\"
                       required/>
            </div>
        </div>
        <!-- End of UserName input -->

        <!-- PetName input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"petname\">Pet name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"petname\" id=\"petname\" placeholder=\"Your Pet Name\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "petname", [], "any", false, false, false, 62), "html", null, true);
        echo "\" required/>
            </div>
        </div>
        <!-- End of petName input -->

        <!-- ownername input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"ownername\">Owner name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"ownername\" id=\"ownername\" placeholder=\"Your name\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "ownername", [], "any", false, false, false, 72), "html", null, true);
        echo "\" required/>
            </div>
        </div>
        <!-- End of ownername input -->

        <!-- phoneNo input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"phoneNo\">Phone No.</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"phoneNo\" id=\"phoneNo\" placeholder=\"Your Phone Number\" value=\"";
        // line 82
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "phoneNo", [], "any", false, false, false, 82), "html", null, true);
        echo "\" required/>
            </div>
        </div>
        <!-- End of phoneNo input -->

        <!-- email input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"email\">E-Mail</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"email\" id=\"email\" placeholder=\"Your E-Mail\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "email", [], "any", false, false, false, 92), "html", null, true);
        echo "\" required/>
            </div>
        </div>
        <!-- End of email input -->

        <!-- pet Radio input-->
        <fieldset class=\"form-group\">
            <div class=\"row\">
                <label class=\"d-none d-sm-block col-sm-3 col-form-label\">Pet</label>
                <!-- left side radio inputs -->
                <div class=\"col-4\">
                    <div class=\"form-check\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"pet\" id=\"dog\" value=\"dog\" checked/>
                        <label class=\"form-check-label\" for=\"dog\">Dog</label>
                    </div>                    
                </div>
                <!-- End of left side radio inputs -->

                <!-- right side radio inputs -->
                <div class=\"col-5\">
                    <div class=\"form-check\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"pet\" id=\"cat\" value=\"cat\"/>
                        <label class=\"form-check-label\" for=\"cat\">Cat</label>
                    </div>                    
                </div>
                <!-- End of right side radio inputs -->
            </div>
        </fieldset>
        <!-- End of pet radio input-->

        <!-- petage input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"age\">Pet age</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"petage\" id=\"petage\" placeholder=\"Your Pet Age\" value=\"";
        // line 127
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "petage", [], "any", false, false, false, 127), "html", null, true);
        echo "\" required/>
            </div>
        </div>
        <!-- End of pettype input -->   

        <!-- password input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"password\">Password</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"Your Password\"  required/>
            </div>
        </div>
        <!-- End of password input -->  

        <!-- repassword input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"repassword\">Password Again</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"password\" class=\"form-control\" name=\"repassword\" id=\"repassword\" placeholder=\"Your Password\"  required/>
            </div>
        </div>
        <!-- End of repassword input -->  

        <!-- Submit button -->
        <div class=\"row \">
            <div class=\"col-12\">
                <button type=\"submit\" class=\"btn btn-secondary w-100\">Register</button>
            </div>            
        </div>
    </form>
    

    <!-- End of Form -->
    <div class=\"row\">
        <div class=\"col-12\">
            <img src=\"./images/staff/dog.png\" alt=\"dogPic\" class=\"w-100\"/>
        </div>
    </div>

 </div>
<!-- End of Container -->             
      
";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 127,  181 => 92,  168 => 82,  155 => 72,  142 => 62,  128 => 51,  115 => 40,  111 => 38,  102 => 36,  98 => 35,  95 => 34,  93 => 33,  82 => 24,  78 => 23,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Register{% endblock %}

{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function() {
            //\$('input[name=username]').blur(function() {
            \$('input[name=username]').on('keyup keypress blur change cut paste', function() {
                var username = \$('input[name=username]').val();
                \$('#isTaken').load(\"/register/isusernametaken/\" + username);
            });
        });
        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>

{% endblock %}

{% block content %}

    <div id=\"registerContainer\" class=\"container\">
        <div class=\"row  justify-content-center\">
            <div class=\"col-6\">
                <img src=\"../images/layout/attempt5.png\" alt=\"dogPic\"  width=\"300\"
\t\t\t\t\t\t height=\"100\"class=\"w-100\"/>
            </div>
        </div><br>
        <div class=\"container\">
            {% if errorsArray %}
                <ul class=\"errorMessage\">
                    {% for error in errorsArray %}
                        <li>{{ error }}</li>
                     {% endfor %}
                </ul>
            {% endif %}
        </div>
<!-- Form for register-->

    <form id=\"resvForm\" class=\"mt-3 needs-validation\" method=\"post\" >

        <!-- UserName input -->
        <div class=\"text-danger\" id=\"isTaken\"></div>
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"username\">User name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"username\" id=\"username\" placeholder=\"Your User Name\" value=\"{{ v.username }}\"
                       required/>
            </div>
        </div>
        <!-- End of UserName input -->

        <!-- PetName input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"petname\">Pet name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"petname\" id=\"petname\" placeholder=\"Your Pet Name\" value=\"{{ v.petname }}\" required/>
            </div>
        </div>
        <!-- End of petName input -->

        <!-- ownername input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"ownername\">Owner name</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"ownername\" id=\"ownername\" placeholder=\"Your name\" value=\"{{ v.ownername }}\" required/>
            </div>
        </div>
        <!-- End of ownername input -->

        <!-- phoneNo input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"phoneNo\">Phone No.</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"phoneNo\" id=\"phoneNo\" placeholder=\"Your Phone Number\" value=\"{{ v.phoneNo }}\" required/>
            </div>
        </div>
        <!-- End of phoneNo input -->

        <!-- email input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"email\">E-Mail</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"email\" id=\"email\" placeholder=\"Your E-Mail\" value=\"{{ v.email }}\" required/>
            </div>
        </div>
        <!-- End of email input -->

        <!-- pet Radio input-->
        <fieldset class=\"form-group\">
            <div class=\"row\">
                <label class=\"d-none d-sm-block col-sm-3 col-form-label\">Pet</label>
                <!-- left side radio inputs -->
                <div class=\"col-4\">
                    <div class=\"form-check\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"pet\" id=\"dog\" value=\"dog\" checked/>
                        <label class=\"form-check-label\" for=\"dog\">Dog</label>
                    </div>                    
                </div>
                <!-- End of left side radio inputs -->

                <!-- right side radio inputs -->
                <div class=\"col-5\">
                    <div class=\"form-check\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"pet\" id=\"cat\" value=\"cat\"/>
                        <label class=\"form-check-label\" for=\"cat\">Cat</label>
                    </div>                    
                </div>
                <!-- End of right side radio inputs -->
            </div>
        </fieldset>
        <!-- End of pet radio input-->

        <!-- petage input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"age\">Pet age</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"text\" class=\"form-control\" name=\"petage\" id=\"petage\" placeholder=\"Your Pet Age\" value=\"{{ v.petage }}\" required/>
            </div>
        </div>
        <!-- End of pettype input -->   

        <!-- password input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"password\">Password</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"Your Password\"  required/>
            </div>
        </div>
        <!-- End of password input -->  

        <!-- repassword input -->
        <div class=\"form-group row align-items-sm-center\">
            <label class=\"d-none d-sm-block col-sm-3 col-form-label\"
                   for=\"repassword\">Password Again</label>
            <div class=\"col-12 col-sm-9\">
                <input type=\"password\" class=\"form-control\" name=\"repassword\" id=\"repassword\" placeholder=\"Your Password\"  required/>
            </div>
        </div>
        <!-- End of repassword input -->  

        <!-- Submit button -->
        <div class=\"row \">
            <div class=\"col-12\">
                <button type=\"submit\" class=\"btn btn-secondary w-100\">Register</button>
            </div>            
        </div>
    </form>
    

    <!-- End of Form -->
    <div class=\"row\">
        <div class=\"col-12\">
            <img src=\"./images/staff/dog.png\" alt=\"dogPic\" class=\"w-100\"/>
        </div>
    </div>

 </div>
<!-- End of Container -->             
      
{% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\ipd20\\petserver\\templates\\register.html.twig");
    }
}
