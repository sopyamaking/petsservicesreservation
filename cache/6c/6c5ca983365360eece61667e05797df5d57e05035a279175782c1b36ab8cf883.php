<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* error_internal.html.twig */
class __TwigTemplate_73f13450a173f95d6482fb7b28c11958e4fb8d08b4251dfe532262e7c1e4992d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "error_internal.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Internal error";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
<p>Internal error occured. Our team of Coding Ninjas has been notified.</p>

<p><a href=\"/\">Click here</a> to continue.</p>

<img src=\"/images/ninja.png\" width=\"200\">

";
    }

    public function getTemplateName()
    {
        return "error_internal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Internal error{% endblock %}

{% block content %}

<p>Internal error occured. Our team of Coding Ninjas has been notified.</p>

<p><a href=\"/\">Click here</a> to continue.</p>

<img src=\"/images/ninja.png\" width=\"200\">

{% endblock %}

", "error_internal.html.twig", "C:\\xampp\\htdocs\\ipd20\\petsclinic\\templates\\error_internal.html.twig");
    }
}
