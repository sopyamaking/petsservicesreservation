<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ajax_productpage.html.twig */
class __TwigTemplate_95f71f8f72e1591c147364b4bbe566c68f61886e36f1a5344e7f20687c6753ba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["productsList"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 2
            echo "     <div class=\"items\">
        <div class=\"prodPicture\">
            <img src=\"";
            // line 4
            echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["product"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["pictureFilePath"] ?? null) : null), "html", null, true);
            echo "\" width=\"300\" height=\"330\">
        </div>
        <div calss=\"prodContent\">
            <h5>";
            // line 7
            echo twig_escape_filter($this->env, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = $context["product"]) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144["name"] ?? null) : null), "html", null, true);
            echo "</h5>  
            <p style=\"font-weight:bold;color:red\">\$";
            // line 8
            echo twig_escape_filter($this->env, (($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b = $context["product"]) && is_array($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b) || $__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b instanceof ArrayAccess ? ($__internal_1cfccaec8dd2e8578ccb026fbe7f2e7e29ac2ed5deb976639c5fc99a6ea8583b["unitPrice"] ?? null) : null), "html", null, true);
            echo "</p>     
            <p>";
            // line 9
            echo twig_escape_filter($this->env, (($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 = $context["product"]) && is_array($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002) || $__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002 instanceof ArrayAccess ? ($__internal_68aa442c1d43d3410ea8f958ba9090f3eaa9a76f8de8fc9be4d6c7389ba28002["description"] ?? null) : null), "html", null, true);
            echo "</p>                 
             <p><button class=\"btn btn-success\">Add to cart</button></p>
            <span style=\"display:none\">";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["product"], "productId", [], "any", false, false, false, 11), "html", null, true);
            echo "</span>
        </div>
     </div>           
 ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 14
            echo "        
    <h3>There is no Product... sorry</h3><br><br>       
                
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "ajax_productpage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  65 => 11,  60 => 9,  56 => 8,  52 => 7,  46 => 4,  42 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for product in productsList %}
     <div class=\"items\">
        <div class=\"prodPicture\">
            <img src=\"{{product['pictureFilePath']}}\" width=\"300\" height=\"330\">
        </div>
        <div calss=\"prodContent\">
            <h5>{{ product['name'] }}</h5>  
            <p style=\"font-weight:bold;color:red\">\${{ product['unitPrice'] }}</p>     
            <p>{{ product['description']}}</p>                 
             <p><button class=\"btn btn-success\">Add to cart</button></p>
            <span style=\"display:none\">{{ product.productId }}</span>
        </div>
     </div>           
 {% else %}        
    <h3>There is no Product... sorry</h3><br><br>       
                
{% endfor %}", "ajax_productpage.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\ajax_productpage.html.twig");
    }
}
