<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin_products_addedit.html.twig */
class __TwigTemplate_aee9a63d1ab284776a843b1c08d3b7f56872c2f22d3c23b24af56e0ab5faf827 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "admin_products_addedit.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Products Add And Edit";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "
    ";
        // line 7
        if (($context["errorsList"] ?? null)) {
            // line 8
            echo "        <ul class=\"errorMessage\">
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorsList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 10
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        </ul>
    ";
        }
        // line 14
        echo "
    <form method=\"post\" enctype=\"multipart/form-data\">
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 16), "html", null, true);
        echo "\"><br><br>
        <p>Please select one category:
        <select id=\"categoryCombox\" name=\"categories\">
            <option value=\"\"></option>
            ";
        // line 20
        if (($context["categories"] ?? null)) {
            echo "            
                ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 22
                echo "                <option value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 22), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 22), "html", null, true);
                echo "</option>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "            
            ";
        }
        // line 25
        echo "        </select></p>
        Description:<br> <textarea cols=50 rows=10 name=\"description\">";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "description", [], "any", false, false, false, 26), "html", null, true);
        echo "</textarea><br><br>
        Price: <input type=\"number\" step=\"0.01\" name=\"price\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "unitPrice", [], "any", false, false, false, 27), "html", null, true);
        echo "\"><br><br>
        Image: <input type=\"file\" name=\"productImage\"><br><br>
        <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "pictureFilePath", [], "any", false, false, false, 29), "html", null, true);
        echo "\" width=\"150\"><br><br>
        <input type=\"submit\" value=\"";
        // line 30
        if (twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "id", [], "any", false, false, false, 30)) {
            echo "Save";
        } else {
            echo "Add";
        }
        echo " product\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "admin_products_addedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 30,  129 => 29,  124 => 27,  120 => 26,  117 => 25,  113 => 23,  102 => 22,  98 => 21,  94 => 20,  87 => 16,  83 => 14,  79 => 12,  70 => 10,  66 => 9,  63 => 8,  61 => 7,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Products Add And Edit{% endblock %}

{% block content %}

    {% if errorsList %}
        <ul class=\"errorMessage\">
            {% for error in errorsList %}
                <li>{{error}}</li>
            {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\" enctype=\"multipart/form-data\">
        Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br><br>
        <p>Please select one category:
        <select id=\"categoryCombox\" name=\"categories\">
            <option value=\"\"></option>
            {% if categories %}            
                {% for category in categories %}
                <option value=\"{{category.id}}\">{{category.name}}</option>
                {% endfor %}            
            {% endif %}
        </select></p>
        Description:<br> <textarea cols=50 rows=10 name=\"description\">{{v.description}}</textarea><br><br>
        Price: <input type=\"number\" step=\"0.01\" name=\"price\" value=\"{{v.unitPrice}}\"><br><br>
        Image: <input type=\"file\" name=\"productImage\"><br><br>
        <img src=\"{{v.pictureFilePath}}\" width=\"150\"><br><br>
        <input type=\"submit\" value=\"{% if v.id %}Save{% else %}Add{% endif %} product\">
    </form>

{% endblock  %}", "admin_products_addedit.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\admin_products_addedit.html.twig");
    }
}
