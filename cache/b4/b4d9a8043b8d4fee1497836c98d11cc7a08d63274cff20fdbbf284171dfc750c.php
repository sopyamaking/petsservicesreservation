<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* master.html.twig */
class __TwigTemplate_b5e37d6f3d875171550d3f1e2c3419a20e84a59615b65f0aef15fb5e848fa1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>        
        <!-- Bootstrap CSS -->
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\"
          integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">    
        <!-- Font Awesome-->
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-1/css/all.min.css\" />
        <!-- Layout CSS -->
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/layout.css\"/>              
        <title>";
        // line 11
        $this->displayBlock('title', $context, $blocks);
        echo " - Fair Pets Vet | Welcome!</title>         
        ";
        // line 12
        $this->displayBlock('head', $context, $blocks);
        // line 13
        echo "    </head>
    <body>
    <!-- Navigation Bar -->
<div id=\"navContainer\" class=\"container-fluid\">
\t<nav id=\"nav\" class=\"navbar navbar-expand-md navbar-light fixed-top\">
\t\t<div class=\"container\">
\t\t\t<a class=\"navbar-brand\" href=\"./doctor/login\">
\t\t\t\t<img src=\"../../../images/layout/attempt3.PNG\" alt=\"Fair Pets Vet Logo\" width=\"100\" height=\"75\">
\t\t\t</a>
\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t</button>
\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t<!-- fix margin -->               
\t\t\t\t<ul class=\"navbar-nav ml-auto\">
\t\t\t\t\t<li class=\"nav-item active ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/\">Home</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/team\">Team</a>
                    </li>                    
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/services\">Services</a>
\t\t\t\t\t</li>\t\t\t\t\t
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/appointment\">Appointments</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/contact\" id=\"resvBtn\">Contact Us</a>
\t\t\t\t\t</li>
                    ";
        // line 43
        if (0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "user", [], "any", false, false, false, 43), "username", [], "any", false, false, false, 43), false)) {
            echo "               
                    <li class=\"nav-item ml-sm-1\">
                        <a class=\"nav-link\" href=\"/login\">Login </a>
                    </li>
                    <li class=\"nav-item ml-sm-1\">
                        <a class=\"nav-link\" href=\"/register\">Register</a>
                    </li>  
                    ";
        }
        // line 50
        echo "  
                    ";
        // line 51
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "user", [], "any", false, false, false, 51), "username", [], "any", false, false, false, 51)) {
            echo "                                               
                    <li class=\"nav-item ml-sm-1\">
                       Welcome,";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "user", [], "any", false, false, false, 53), "username", [], "any", false, false, false, 53), "html", null, true);
            echo "!
                    </li>
                    <li class=\"nav-item ml-sm-1\">
                        <a class=\"nav-link\" href=\"/logout\">Logout</a>
                    </li>    
                    ";
        }
        // line 58
        echo "   
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</nav>
</div>
<!-- End of Navigation Bar -->



      
            <div id=\"content\">";
        // line 69
        $this->displayBlock('content', $context, $blocks);
        // line 70
        echo "</div>


        <div id=\"footer\">              
                ";
        // line 74
        $this->displayBlock('footer', $context, $blocks);
        // line 119
        echo "            </div>
                   
    </body>
</html>";
    }

    // line 11
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Default";
    }

    // line 12
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 69
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 70
        echo "            ";
    }

    // line 74
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 75
        echo "                    <div class=\"container\">
                        <div class=\"row text-center\">
                            <div class=\"col-md-4\">
                                <hr class=\"light\">
                                <h5>Location</h5>
                                <hr class=\"light\">
                                <p>T:555-555-5555</p>
                                <p>genericEmail@hotmail.com</p>
                                <p>3818 BlackWell Street</p>
                                <p>Fairbanks, Alaska, 99504 </p>
                            </div>
                            <div class=\"col-md-4\">
                            <br>
                                <img src=\"../../../images/layout/attempt4.png\" alt=\"footerLogo\" width=\"180\" height=\"180\">
                            </div>
                            <div class=\"col-md-4\">
                                <hr class=\"light\">
                                <h5>Our Schedule</h5>
                                <hr class=\"light\">
                                <p>Mon-Fri: 9am - 7pm</p>
                                <p>Saturday: 10am - 3pm</p>
                                <p>Sunday: 9am - 1pm</p>
                            </div>
                        </div>

        <!-- Social Medias / Find better way maybe? -->
                        <div class=\"row\">
                            <div class=\"col-12\">
                                <hr class=\"light-100\">
                            </div>


                            <div class=\"col\">
                                <p class=\"footer-bottom m-0 text-right\">&copy; Sherwood Park Animal Hospital</p>
                            </div>
                            <div class=\"col d-flex align-items-center\">
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-facebook footer-bottom\"></i></a>
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-twitter footer-bottom\"></i></a>
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-instagram footer-bottom\"></i></a>
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-youtube footer-bottom\"></i></a>
                            </div>
                        </div>
                    </div>
                ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 75,  170 => 74,  166 => 70,  162 => 69,  156 => 12,  149 => 11,  142 => 119,  140 => 74,  134 => 70,  132 => 69,  119 => 58,  110 => 53,  105 => 51,  102 => 50,  91 => 43,  59 => 13,  57 => 12,  53 => 11,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>        
        <!-- Bootstrap CSS -->
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\"
          integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">    
        <!-- Font Awesome-->
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-1/css/all.min.css\" />
        <!-- Layout CSS -->
\t    <link rel=\"stylesheet\" type=\"text/css\" href=\"/css/layout.css\"/>              
        <title>{% block title %}Default{% endblock %} - Fair Pets Vet | Welcome!</title>         
        {% block head %}{% endblock %}
    </head>
    <body>
    <!-- Navigation Bar -->
<div id=\"navContainer\" class=\"container-fluid\">
\t<nav id=\"nav\" class=\"navbar navbar-expand-md navbar-light fixed-top\">
\t\t<div class=\"container\">
\t\t\t<a class=\"navbar-brand\" href=\"./doctor/login\">
\t\t\t\t<img src=\"../../../images/layout/attempt3.PNG\" alt=\"Fair Pets Vet Logo\" width=\"100\" height=\"75\">
\t\t\t</a>
\t\t\t<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
\t\t\t\t<span class=\"navbar-toggler-icon\"></span>
\t\t\t</button>
\t\t\t<div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t\t\t<!-- fix margin -->               
\t\t\t\t<ul class=\"navbar-nav ml-auto\">
\t\t\t\t\t<li class=\"nav-item active ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/\">Home</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/team\">Team</a>
                    </li>                    
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/services\">Services</a>
\t\t\t\t\t</li>\t\t\t\t\t
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/appointment\">Appointments</a>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"nav-item ml-sm-1\">
\t\t\t\t\t\t<a class=\"nav-link\" href=\"/contact\" id=\"resvBtn\">Contact Us</a>
\t\t\t\t\t</li>
                    {% if session.user.username ==false %}               
                    <li class=\"nav-item ml-sm-1\">
                        <a class=\"nav-link\" href=\"/login\">Login </a>
                    </li>
                    <li class=\"nav-item ml-sm-1\">
                        <a class=\"nav-link\" href=\"/register\">Register</a>
                    </li>  
                    {% endif %}  
                    {% if session.user.username %}                                               
                    <li class=\"nav-item ml-sm-1\">
                       Welcome,{{ session.user.username }}!
                    </li>
                    <li class=\"nav-item ml-sm-1\">
                        <a class=\"nav-link\" href=\"/logout\">Logout</a>
                    </li>    
                    {% endif %}   
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t</nav>
</div>
<!-- End of Navigation Bar -->



      
            <div id=\"content\">{% block content %}
            {% endblock %}</div>


        <div id=\"footer\">              
                {% block footer %}
                    <div class=\"container\">
                        <div class=\"row text-center\">
                            <div class=\"col-md-4\">
                                <hr class=\"light\">
                                <h5>Location</h5>
                                <hr class=\"light\">
                                <p>T:555-555-5555</p>
                                <p>genericEmail@hotmail.com</p>
                                <p>3818 BlackWell Street</p>
                                <p>Fairbanks, Alaska, 99504 </p>
                            </div>
                            <div class=\"col-md-4\">
                            <br>
                                <img src=\"../../../images/layout/attempt4.png\" alt=\"footerLogo\" width=\"180\" height=\"180\">
                            </div>
                            <div class=\"col-md-4\">
                                <hr class=\"light\">
                                <h5>Our Schedule</h5>
                                <hr class=\"light\">
                                <p>Mon-Fri: 9am - 7pm</p>
                                <p>Saturday: 10am - 3pm</p>
                                <p>Sunday: 9am - 1pm</p>
                            </div>
                        </div>

        <!-- Social Medias / Find better way maybe? -->
                        <div class=\"row\">
                            <div class=\"col-12\">
                                <hr class=\"light-100\">
                            </div>


                            <div class=\"col\">
                                <p class=\"footer-bottom m-0 text-right\">&copy; Sherwood Park Animal Hospital</p>
                            </div>
                            <div class=\"col d-flex align-items-center\">
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-facebook footer-bottom\"></i></a>
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-twitter footer-bottom\"></i></a>
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-instagram footer-bottom\"></i></a>
                                <a href=\"#\" class=\"ml-sm-4 ml-2\"><i class=\"fab fa-youtube footer-bottom\"></i></a>
                            </div>
                        </div>
                    </div>
                {% endblock %}
            </div>
                   
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\ipd20\\petserver\\templates\\master.html.twig");
    }
}
