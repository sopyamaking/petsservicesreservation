<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin_products_delete.html.twig */
class __TwigTemplate_764cfb0ebc3c9f40b1709f25d185ba46d36b3002844439015d10f31fd9ca762f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "admin_products_delete.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Product Detail";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    
    <form method=\"post\" >
        Name: <input type=\"text\" name=\"name\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "name", [], "any", false, false, false, 8), "html", null, true);
        echo "\"><br><br>
        category ID: <input type=\"text\" name=\"categoryId\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "categoryId", [], "any", false, false, false, 9), "html", null, true);
        echo "\"><br> <br>       
        Description:<br>
         <textarea cols=50 rows=10 name=\"description\">";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "description", [], "any", false, false, false, 11), "html", null, true);
        echo "</textarea><br><br>
        Price: <input type=\"number\" step=\"0.01\" name=\"price\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "unitPrice", [], "any", false, false, false, 12), "html", null, true);
        echo "\"><br><br>
        <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["v"] ?? null), "pictureFilePath", [], "any", false, false, false, 13), "html", null, true);
        echo "\" width=\"150\"><br><br>       
        If you want to delete it, please click <input type=\"submit\" value=\"confirm\"> or you can <a href=\"/admin/products/list\">Click here to return</a>
    </form>
    
";
    }

    public function getTemplateName()
    {
        return "admin_products_delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 13,  75 => 12,  71 => 11,  66 => 9,  62 => 8,  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Product Detail{% endblock %}

{% block content %}
    
    <form method=\"post\" >
        Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br><br>
        category ID: <input type=\"text\" name=\"categoryId\" value=\"{{v.categoryId}}\"><br> <br>       
        Description:<br>
         <textarea cols=50 rows=10 name=\"description\">{{v.description}}</textarea><br><br>
        Price: <input type=\"number\" step=\"0.01\" name=\"price\" value=\"{{v.unitPrice}}\"><br><br>
        <img src=\"{{v.pictureFilePath}}\" width=\"150\"><br><br>       
        If you want to delete it, please click <input type=\"submit\" value=\"confirm\"> or you can <a href=\"/admin/products/list\">Click here to return</a>
    </form>
    
{% endblock content %}", "admin_products_delete.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\admin_products_delete.html.twig");
    }
}
