<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ajax_index.html.twig */
class __TwigTemplate_dcceaba35998bfc5fb495975f5f9d719a27f718bfe6d62139846d0af160e2dea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'head' => [$this, 'block_head'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "ajax_index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Home-store";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> 
    <script>
        var currentPage = 1;
        function loadProductPage(page) {
            page = page < 1 ? 1 : page; // min is 1
            page = page > ";
        // line 11
        echo twig_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo " ? ";
        echo twig_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo " : page; // max is ";
        echo twig_escape_filter($this->env, ($context["totalPages"] ?? null), "html", null, true);
        echo "            
            
            // remove style pageLinkCurrent on currentPage
            \$(\"#pageLink\"+currentPage).removeClass(\"pageLinkCurrent\"); 
            currentPage = page;           
            \$(\"#pageLink\"+currentPage).addClass(\"pageLinkCurrent\");
            \$(\"#productsListHolder\").load(\"/ajax/productpage/\" + page);
        }

        \$(document).ready(function(){
            loadProductPage(currentPage);
        });

        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>
";
    }

    // line 31
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "        <h2 style=\"text-align:center\">Products</h2><br />
        <hr />
        <div class=\"container\">     
            ";
        // line 35
        if (($context["categories"] ?? null)) {
            // line 36
            echo "                <div class=\"category\">
                    <ul>
                    ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 39
                echo "                        <li><a href=\"http://day06eshop.ipd20:8888/category/";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", [], "any", false, false, false, 39), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = $context["category"]) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["name"] ?? null) : null), "html", null, true);
                echo "</a></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "   
                    </ul>                 
                </div>
            ";
        }
        // line 44
        echo "
            
            <div id=\"productsListHolder\">
            </div>
           
        </div>
             <div class=\"pageDiv\">               
                    <span class=\"pageNav\" onclick=\"loadProductPage(currentPage-1);\"> Previous </span>                

                ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, ($context["totalPages"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
            echo "                   
                        <span id=\"pageLink";
            // line 54
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo "\" class=\"pageLink\" onclick=\"loadProductPage(";
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo ");\">";
            echo twig_escape_filter($this->env, $context["page"], "html", null, true);
            echo "</span>                  
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "                
                    <span class=\"pageNav\" onclick=\"loadProductPage(currentPage+1);\"> Next </span>
                
            </div>  
        
";
    }

    public function getTemplateName()
    {
        return "ajax_index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 56,  146 => 54,  140 => 53,  129 => 44,  123 => 40,  112 => 39,  108 => 38,  104 => 36,  102 => 35,  97 => 32,  93 => 31,  66 => 11,  59 => 6,  55 => 5,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Home-store{% endblock %}

{% block head %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> 
    <script>
        var currentPage = 1;
        function loadProductPage(page) {
            page = page < 1 ? 1 : page; // min is 1
            page = page > {{totalPages}} ? {{totalPages}} : page; // max is {{ totalPages }}            
            
            // remove style pageLinkCurrent on currentPage
            \$(\"#pageLink\"+currentPage).removeClass(\"pageLinkCurrent\"); 
            currentPage = page;           
            \$(\"#pageLink\"+currentPage).addClass(\"pageLinkCurrent\");
            \$(\"#productsListHolder\").load(\"/ajax/productpage/\" + page);
        }

        \$(document).ready(function(){
            loadProductPage(currentPage);
        });

        \$(document).ajaxError(function(event, jqxhr, settings, thrownError) {
            console.log(\"Ajax error occured on \" + settings.url);
            alert(\"Ajax error occured\");
        });
    </script>
{% endblock  %}

{% block  content %}
        <h2 style=\"text-align:center\">Products</h2><br />
        <hr />
        <div class=\"container\">     
            {% if categories %}
                <div class=\"category\">
                    <ul>
                    {% for category in categories %}
                        <li><a href=\"http://day06eshop.ipd20:8888/category/{{category.id}}\">{{ category['name'] }}</a></li>
                    {% endfor %}   
                    </ul>                 
                </div>
            {% endif %}

            
            <div id=\"productsListHolder\">
            </div>
           
        </div>
             <div class=\"pageDiv\">               
                    <span class=\"pageNav\" onclick=\"loadProductPage(currentPage-1);\"> Previous </span>                

                {% for page in 1 .. totalPages %}                   
                        <span id=\"pageLink{{page}}\" class=\"pageLink\" onclick=\"loadProductPage({{page}});\">{{page}}</span>                  
                {% endfor %}
                
                    <span class=\"pageNav\" onclick=\"loadProductPage(currentPage+1);\"> Next </span>
                
            </div>  
        
{% endblock %}", "ajax_index.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\ajax_index.html.twig");
    }
}
