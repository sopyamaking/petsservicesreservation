<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* category_add_success.html.twig */
class __TwigTemplate_8430f7cb7febf25c9c1a2c1126a04209edfd8db7053aaf68314d19b0df2ec396 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "category_add_success.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Category Add ";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <p>Category : ";
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo " has been added successful! </p>
    <p><a href=\"/\">Click here to Home</a></p>
";
    }

    public function getTemplateName()
    {
        return "category_add_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Category Add {% endblock %}

{% block content %}
    <p>Category : {{name}} has been added successful! </p>
    <p><a href=\"/\">Click here to Home</a></p>
{% endblock %}", "category_add_success.html.twig", "C:\\xampp\\htdocs\\ipd20\\day06eshop\\templates\\category_add_success.html.twig");
    }
}
