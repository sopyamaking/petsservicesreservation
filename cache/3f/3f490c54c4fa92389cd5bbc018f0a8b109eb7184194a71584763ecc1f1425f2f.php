<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_e989d30591416b76ed4979b3d2a20be296a126417f9db421860e5fb9a72a72ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Home";
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "               <!-- Carousel Image Slider / Not Done CSS -->
<div id=\"indexContainer\" class=\"container-fluid\">
\t<div class=\"container\">
\t\t<div id=\"slides\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t<!-- indicators -->
\t\t\t<ul class=\"carousel-indicators\">
\t\t\t\t<li data-target=\"#slides\" data-slide-to=\"0\" class=\"active\"></li>\t\t\t\t
\t\t\t</ul>
\t\t\t<div class=\"carousel-inner\">
\t\t\t\t<div class=\"carousel-item active\">
\t\t\t\t\t<img src=\"./images/index/pugInBlanky.jpg\" alt=\"First Image\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<h3 class=\"display-2\">Welcome!</h1>
                        <button type=\"button\" class=\"btn btn-outline-light btn-lg\"><a href=\"/appointment\" class=\"text-decoration-none text-light\">Make an Appointment</a></button>
\t\t\t\t\t</div>
\t\t\t\t</div>\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
</div>
        <!-- Jumbotron with about us info? -->
<div id=\"aboutUs\" class=\"container-fluid\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-12 col-md-9 col-xl-10\">
\t\t\t\t<h2>About Us</h2>
\t\t\t\t<p class=\"bigFont\">We are a veterinary hospital that has
\t\t\t\t\tbeen in the business for the past 25 years.
\t\t\t\t\tOur love and care for animals has been growing
\t\t\t\t\tever since we began. We offer a number of services
\t\t\t\t\tfor your beloved companion. Your pet's emotional health 
\t\t\t\t\tis our priority at all times. If you have a concern 
\t\t\t\t\tabout your pet and his/her experience at our hospital,
\t\t\t\t\t please feel free to call us at any time and we would 
\t\t\t\t\t be happy to discuss this with you prior to your visit.</p>
\t\t\t</div>

\t\t\t<div class=\"col-12 col-md-3 col-xl-2 text-center\">
\t\t\t\t<div class=\"card\" style=\"padding:5px 5px 0px 5px;background-color:#bdbdbd;\">
\t\t\t\t\t<img class=\"card-img-top card-height\" src=\"./images/index/dogStick.jpg\" alt=\"Adult Dog Food Picture\" width=\"150\"
\t\t\t\t\t\t height=\"150\">\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<!-- Our Staff -->
<div class=\"container-fluid\">
\t<div class=\"container\">
\t\t<h1 class=\"text-center display-4\">Our Staff</h1>
\t\t<hr class=\"staff-hr\">

\t\t<!-- row 1 of staff -->
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr16.jpg\" alt=\"Staff Member #16\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Heather Randall</h4>
\t\t\t\t\t\t<p class=\"card-text\">Animal Health Technician
\t\t\t\t\t\tCaht Vanier College</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Heather</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr14.jpg\" alt=\"Staff Member #14\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Gemma Stevens</h4>
\t\t\t\t\t\t<p class=\"card-text\">Animal Care/Veterinary Assistant</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Gemma</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr15.jpg\" alt=\"Staff Member #15\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Geneviere Beauchamp</h4>
\t\t\t\t\t\t<p class=\"card-text\">Veterinarian
\t\t\t\t\t\t\tDVM University of Montreal</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Geneviere</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- row 2 of staff -->
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr26.jpg\" alt=\"Staff Member #26\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Nancy Duguay</h4>
\t\t\t\t\t\t<p class=\"card-text\">Veterinarian
\t\t\t\t\t\t\tDVM University of Montreal</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Nancy</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr6.jpg\" alt=\"Staff Member #6\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Yannick Massicotte</h4>
\t\t\t\t\t\t<p class=\"card-text\">Hospital Manager and Owner MSc University of McGi...</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Yannick</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr10.jpg\" alt=\"Staff Member #10\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Amanda Morton</h4>
\t\t\t\t\t\t<p class=\"card-text\">Animal Care / Veterinary Assistant (Lead)</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Amanda</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<!-- Services -->
<div class=\"container-fluid\">
\t<div id=\"services\" class=\"container\">
\t\t<div class=\"row \">
\t\t\t<div class=\"col-md-9\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"text-center col-12 text-left\">
\t\t\t\t\t\t<h1 class=\"display-4\">Services</h1>
\t\t\t\t\t\t<hr class=\"services-hr\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"text-center col-12 col-md-6\">
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Prevention</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Radiographie</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Surgery </h3></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"text-center col-12 col-md-6\">
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>General Clinic</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>MicroChipping</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Other</h3></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\" col-12 col-md-3  text-center\">
\t\t\t\t<div class=\"card\" style=\"padding:5px 5px 0px 5px;background-color:#bdbdbd;\">
\t\t\t\t\t<img class=\"card-img-top card-height\" src=\"./images/index/kittens.jpg\" alt=\"Cat Picture\" width=\"125\"
\t\t\t\t\t\t height=\"150\">
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>


";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 6,  54 => 5,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"master.html.twig\" %}

{% block title %}Home{% endblock %}

{% block content %}
               <!-- Carousel Image Slider / Not Done CSS -->
<div id=\"indexContainer\" class=\"container-fluid\">
\t<div class=\"container\">
\t\t<div id=\"slides\" class=\"carousel slide\" data-ride=\"carousel\">
\t\t\t<!-- indicators -->
\t\t\t<ul class=\"carousel-indicators\">
\t\t\t\t<li data-target=\"#slides\" data-slide-to=\"0\" class=\"active\"></li>\t\t\t\t
\t\t\t</ul>
\t\t\t<div class=\"carousel-inner\">
\t\t\t\t<div class=\"carousel-item active\">
\t\t\t\t\t<img src=\"./images/index/pugInBlanky.jpg\" alt=\"First Image\">
\t\t\t\t\t<div class=\"carousel-caption d-none d-md-block\">
\t\t\t\t\t\t<h3 class=\"display-2\">Welcome!</h1>
                        <button type=\"button\" class=\"btn btn-outline-light btn-lg\"><a href=\"/appointment\" class=\"text-decoration-none text-light\">Make an Appointment</a></button>
\t\t\t\t\t</div>
\t\t\t\t</div>\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
</div>
        <!-- Jumbotron with about us info? -->
<div id=\"aboutUs\" class=\"container-fluid\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-12 col-md-9 col-xl-10\">
\t\t\t\t<h2>About Us</h2>
\t\t\t\t<p class=\"bigFont\">We are a veterinary hospital that has
\t\t\t\t\tbeen in the business for the past 25 years.
\t\t\t\t\tOur love and care for animals has been growing
\t\t\t\t\tever since we began. We offer a number of services
\t\t\t\t\tfor your beloved companion. Your pet's emotional health 
\t\t\t\t\tis our priority at all times. If you have a concern 
\t\t\t\t\tabout your pet and his/her experience at our hospital,
\t\t\t\t\t please feel free to call us at any time and we would 
\t\t\t\t\t be happy to discuss this with you prior to your visit.</p>
\t\t\t</div>

\t\t\t<div class=\"col-12 col-md-3 col-xl-2 text-center\">
\t\t\t\t<div class=\"card\" style=\"padding:5px 5px 0px 5px;background-color:#bdbdbd;\">
\t\t\t\t\t<img class=\"card-img-top card-height\" src=\"./images/index/dogStick.jpg\" alt=\"Adult Dog Food Picture\" width=\"150\"
\t\t\t\t\t\t height=\"150\">\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
<!-- Our Staff -->
<div class=\"container-fluid\">
\t<div class=\"container\">
\t\t<h1 class=\"text-center display-4\">Our Staff</h1>
\t\t<hr class=\"staff-hr\">

\t\t<!-- row 1 of staff -->
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr16.jpg\" alt=\"Staff Member #16\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Heather Randall</h4>
\t\t\t\t\t\t<p class=\"card-text\">Animal Health Technician
\t\t\t\t\t\tCaht Vanier College</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Heather</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr14.jpg\" alt=\"Staff Member #14\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Gemma Stevens</h4>
\t\t\t\t\t\t<p class=\"card-text\">Animal Care/Veterinary Assistant</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Gemma</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr15.jpg\" alt=\"Staff Member #15\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Geneviere Beauchamp</h4>
\t\t\t\t\t\t<p class=\"card-text\">Veterinarian
\t\t\t\t\t\t\tDVM University of Montreal</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Geneviere</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<!-- row 2 of staff -->
\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr26.jpg\" alt=\"Staff Member #26\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Nancy Duguay</h4>
\t\t\t\t\t\t<p class=\"card-text\">Veterinarian
\t\t\t\t\t\t\tDVM University of Montreal</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Nancy</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr6.jpg\" alt=\"Staff Member #6\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Yannick Massicotte</h4>
\t\t\t\t\t\t<p class=\"card-text\">Hospital Manager and Owner MSc University of McGi...</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Yannick</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card h-100\">
\t\t\t\t\t<img class=\"card-img-top\" src=\"./images/staff/dr10.jpg\" alt=\"Staff Member #10\">
\t\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t\t<h4 class=\"card-title\">Amanda Morton</h4>
\t\t\t\t\t\t<p class=\"card-text\">Animal Care / Veterinary Assistant (Lead)</p>
\t\t\t\t\t\t<a href=\"/team\" class=\"btn btn-outline-secondary\">Meet Amanda</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<!-- Services -->
<div class=\"container-fluid\">
\t<div id=\"services\" class=\"container\">
\t\t<div class=\"row \">
\t\t\t<div class=\"col-md-9\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"text-center col-12 text-left\">
\t\t\t\t\t\t<h1 class=\"display-4\">Services</h1>
\t\t\t\t\t\t<hr class=\"services-hr\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"text-center col-12 col-md-6\">
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Prevention</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Radiographie</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Surgery </h3></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"text-center col-12 col-md-6\">
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>General Clinic</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>MicroChipping</h3></a>
\t\t\t\t\t\t<a href=\"/services\">
\t\t\t\t\t\t\t<h3>Other</h3></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\" col-12 col-md-3  text-center\">
\t\t\t\t<div class=\"card\" style=\"padding:5px 5px 0px 5px;background-color:#bdbdbd;\">
\t\t\t\t\t<img class=\"card-img-top card-height\" src=\"./images/index/kittens.jpg\" alt=\"Cat Picture\" width=\"125\"
\t\t\t\t\t\t height=\"150\">
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>


{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\ipd20\\petserver\\templates\\index.html.twig");
    }
}
