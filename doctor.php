<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


require_once "setup.php";

$app->get("/doctor/availibility/add/{id:[0-9]+}", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $doctorId = $args['id'];   
    /*if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }*/
    $doctor = DB::queryFirstRow("SELECT * FROM doctors AS d                     
                                   WHERE d.id=:i",$doctorId);
    $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate,av.startTime,av.endTime,a.description,p.petName,p.petType,p.age AS petAge
                                    FROM appointments AS a 
                                    join availibilities  as av on a.availibilityId = av.id 
                                    Join pets AS p  On a.petId = p.id                                    
                                   WHERE a.doctorId=:i", $doctorId);
    $availableDate = [date('Y-m-d ',strtotime('+1 day')),date('Y-m-d ',strtotime('+2 day')),
    date('Y-m-d ',strtotime('+3 day')),date('Y-m-d ',strtotime('+4 day')),date('Y-m-d ',strtotime('+5 day')),date('Y-m-d ',strtotime('+6 day')),date('Y-m-d ',strtotime('+7 day'))];
    return $view->render($response, 'doctors.html.twig',['doctor'=>$doctor, 'availableDate'=>$availableDate,'appointmentsList'=>$appointmentsList]);
});

$app->get('/doctor/availibility/{doctorid}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $doctorId = $args['doctorid'];
    $alldays= array();
    $date = new DateTime("now", new DateTimeZone('America/New_York') );
    for($x = 0; $x < 7; $x++){
        $date = $date->modify('+1 day');
        $alldays[] = $date->format("Y-m-d");
    }
    $availibilities = array ();
    for($y=9;$y<17;$y++){
        $from = $y.":00";
        $to = ($y+1).":00";
        $curdate = new DateTime("now", new DateTimeZone('America/New_York') );
        $days = array ();
        global $log;
        
        for($x = 0; $x < 7; $x++){
            $curdate = $curdate->modify('+1 day');
            $strDate = $curdate->format("Y-m-d");
            $hour = DB::queryFirstRow("SELECT * FROM availibilities WHERE doctorId=:i AND availableDate=:s AND startTime=:s AND endTime=:s",
                                    $doctorId,$strDate,$from,$to);           
            $log->debug("date:".$strDate."/from:".$from."/to:".$to);
            if($hour){
                $days[]=1;// means true
            }else{
                $days[]=0;// means false
            }
        }
        $availibilities[]=$days;
    }
    $doctor = DB::queryFirstRow("SELECT * FROM doctors AS d                     
                                   WHERE d.id=:i",$doctorId);
    $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate,av.startTime,av.endTime,a.description,p.petName,p.petType,p.age AS petAge
                                    FROM appointments AS a 
                                    join availibilities  as av on a.availibilityId = av.id  
                                    Join pets AS p On a.petId = p.id                                    
                                   WHERE a.doctorId=:i", $doctorId);
    return $view->render($response, 'doctors.html.twig',['doctor'=>$doctor,'availibilities'=>$availibilities,'days'=>$alldays,'appointmentsList'=>$appointmentsList,'doctorId'=>$doctorId]);
});

$app->post('/doctor/availibilities', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postvars = $request->getParsedBody();
    $doctorId = $postvars['doctorId'];

    foreach ($postvars as $key => $value){
       //echo "key:".$key ." value:".$value."<br>";
        $availableDate = substr($value,0,10);
        $startTime = substr($value,11,strlen($value)-11).':00';
        $endTime = (substr($value,11,strlen($value)-11)+1).':00';
        $hour = DB::queryFirstRow("SELECT * FROM availibilities WHERE doctorId=:i AND availableDate=:s AND startTime=:s AND endTime=:s",
                                    $doctorId,$availableDate,$startTime,$endTime ); 
        if (!$hour && $key != 'doctorId') {
            DB::insert('availibilities',['doctorId'=>$doctorId,'availableDate'=>$availableDate,'startTime'=>$startTime,'endTime'=>$endTime]);            
        }
    }
        $alldays= array();
            $date = new DateTime("now", new DateTimeZone('America/New_York') );
            for($x = 0; $x < 7; $x++){
                $date = $date->modify('+1 day');
                $alldays[] = $date->format("Y-m-d");
            }
            $availibilities = array ();
            for($y=9;$y<17;$y++){
                $from = $y.":00";
                $to = ($y+1).":00";
                $curdate = new DateTime("now", new DateTimeZone('America/New_York') );
                $days = array ();
                global $log;
        
                for($x = 0; $x < 7; $x++){
                    $curdate = $curdate->modify('+1 day');
                    $strDate = $curdate->format("Y-m-d");
                    $hour = DB::queryFirstRow("SELECT * FROM availibilities WHERE doctorId=:i AND availableDate=:s AND startTime=:s AND endTime=:s",
                                    $doctorId,$strDate,$from,$to);           
                    $log->debug("date:".$strDate."/from:".$from."/to:".$to);
                    if($hour){
                        $days[]=1;// means true
                    }else{
                        $days[]=0;// means false
                    }
                }
                $availibilities[]=$days;
            }
            $doctor = DB::queryFirstRow("SELECT * FROM doctors AS d                     
                                   WHERE d.id=:i",$doctorId);
            $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate,av.startTime,av.endTime,a.description,p.petName,p.petType,p.age AS petAge
                                            FROM appointments AS a  
                                            join availibilities  as av on a.availibilityId = av.id 
                                            Join pets AS p  On a.petId = p.id                                    
                                            WHERE a.doctorId=:i", $doctorId);
            return $view->render($response, 'doctors.html.twig',['doctor'=>$doctor,'availibilities'=>$availibilities,'days'=>$alldays,'appointmentsList'=>$appointmentsList,'doctorId'=>$doctorId]);
        
});


