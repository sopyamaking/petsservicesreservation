<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

session_start();


require __DIR__ . '/vendor/autoload.php';

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {
    // hosting on ipd20.com
    DB::$user = 'cp4966_sophy';
    DB::$password = '46O2XchmGXtFgZJX';
    DB::$dbName = 'cp4966_petclinic';
} else { // local computer
    DB::$user = 'root';
    DB::$password = '';
    DB::$dbName = 'petclinic';
    DB::$port = 3333;
}
    DB::$param_char = ':';

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    header("Location: /error_internal");
    global $log;
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}

$container = new Container();
$container->set('upload_directory', __DIR__ . '/images/staff');
AppFactory::setContainer($container);

$app = AppFactory::create();

$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache','debug'=>true]);


$twig->getEnvironment()->addGlobal('session', $_SESSION);

$app->addErrorMiddleware(true,true,true);
// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));