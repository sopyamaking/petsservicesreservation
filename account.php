<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


require_once "setup.php";


$app->get('/register/isusernametaken/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $username = isset($args['username']) ? $args['username'] : "";
    $user = DB::queryFirstRow("SELECT id FROM pets WHERE username=:s", $username);
    return $view->render($response, 'register_isusernametaken.html.twig', ['isTaken' => ($user != null) ]);
});

$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

$app->post('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $username = $postVars['username']; 
    $petname = $postVars['petname'];
    $ownername = $postVars['ownername'];
    $phoneNo = $postVars['phoneNo'];    
    $email = $postVars['email'];
    $pettype = $postVars['pet'];
    $petage = $postVars['petage'];
    $password = $postVars['password'];    
    $repassword = $postVars['repassword'];
    
    $errorsArray = array();

    if(strlen($username)<6 || strlen($username) > 20 || !preg_match('/^[a-zA-Z][a-zA-Z0-9_]{5,19}$/',$username)){
        array_push($errorsArray,"Username must be 6-20 characters long, begin with a letter and only "
        . "consist of uppercase/lowercase letters, digits, and underscores!");
        $postVars ['username'] = "";
    }else{
        $user = DB::queryFirstRow("SELECT * FROM pets WHERE username=:s", $username);
        if ($user) {
            array_push($errorsArray, "Username already registered, try a different one");
            $postVars['username'] = "";
        }
    }
    if(strlen($petname)<3 || strlen($petname) > 20 || !preg_match('/^[a-zA-Z][a-zA-Z0-9_]{2,19}$/',$petname)){
        array_push($errorsArray,"Petname must be 3-20 characters long, begin with a letter and only "
        . "consist of uppercase/lowercase letters, digits, and underscores!");
        $postVars ['petname'] = "";
    }
    if(strlen($ownername)<3 || strlen($ownername) > 20 || !preg_match('/^[a-zA-Z][a-zA-Z0-9_]{2,19}$/',$ownername)){
            array_push($errorsArray,"Ownername must be 3-20 characters long, begin with a letter and only "
            . "consist of uppercase/lowercase letters, digits, and underscores!");
            $postVars ['ownername'] = "";
    }
    if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE ){
        array_push($errorsArray,"Error: Email does not look valid");
        $postVars ['email'] = "";
    }    
    if($petage<1 || $petage > 20){
        array_push($errorsArray,"petage must be between 1-20!"  );
        $postVars ['petage'] = "";
    }
    if ($password != $repassword) {
        array_push($errorsArray,"Passwords do not match");        
    } else {
        if ((strlen($password) < 6)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
                    array_push($errorsArray, "Password must be at least 6 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
        }
    } 
    if($errorsArray){
        return $view->render($response, 'register.html.twig', ['v'=>$postVars,'errorsArray' => $errorsArray]);
    }else{
        DB::insert('pets',['petName'=>$petname,'petType'=>$pettype,'age'=>$petage,'ownerName'=>$ownername,'ownerPhone'=>$phoneNo,'ownerEmail'=>$email,'username'=>$username, 'password'=>$password]);
        return $response->withHeader('Location', '/');       
    }
});

$app->get('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'login.html.twig');
});


$app->post('/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $password = $postVars['password'];     
    try{
        $user = DB::queryFirstRow("SELECT * FROM pets WHERE username=:s", $username);
    }catch(Exception $e){
        echo $e->getMessage();
        die();
    }
    // check validity
    $loginSuccessful = false;    
    if($user){
        if ($user['password'] == $password) {
            $loginSuccessful = true;                  
        }    
    }
    if(!$loginSuccessful){
        return $view->render($response, 'login.html.twig', ['error' => true ]);
    }else{
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;    
        return $response->withHeader('Location', '/');    
    }
});

$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']); 
    return $response->withHeader('Location', '/');    
});

$app->get('/doctor/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'doctor_admin_login.html.twig');
});

$app->post('/doctor/login', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);    
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $password = $postVars['password'];     
    try{
        $user = DB::queryFirstRow("SELECT * FROM doctors WHERE username=:s", $username);
    }catch(Exception $e){
        echo $e->getMessage();
        die();
    }
    // check validity
    $loginSuccessful = false;    
    if($user){
        if ($user['password'] == $password) {
            $loginSuccessful = true;                  
        }    
    }
    if(!$loginSuccessful){
        return $view->render($response, 'login.html.twig', ['error' => true ]);
    }else{
        if($user['isAdmin']== 'true'){
           unset($user['password']); // remove password from array for security reasons
            $_SESSION['user'] = $user;    
            return $response->withHeader('Location', '/admin/appointments'); 
        }else{
            $id = $user['id'];
            unset($user['password']); // remove password from array for security reasons
            $_SESSION['user'] = $user;    
            return $response->withHeader('Location', '/doctor/availibility/'.$id);
        }
            
    }
});