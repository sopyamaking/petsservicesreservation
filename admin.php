<?php
use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


require_once "setup.php";

$app->get('/doctorAdd/isusernametaken/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $username = isset($args['username']) ? $args['username'] : "";
    $user = DB::queryFirstRow("SELECT id FROM doctors WHERE username=:s", $username);
    return $view->render($response, 'register_isusernametaken.html.twig', ['isTaken' => ($user != null) ]);
});

$app->get('/petAdd/isusernametaken/[{username}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $username = isset($args['username']) ? $args['username'] : "";
    $user = DB::queryFirstRow("SELECT id FROM pets WHERE username=:s", $username);
    return $view->render($response, 'register_isusernametaken.html.twig', ['isTaken' => ($user != null) ]);
});

$app->get('/serviceAdd/isusernametaken/[{name}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $name = isset($args['name']) ? $args['name'] : "";
    $service = DB::queryFirstRow("SELECT id FROM services WHERE name=:s", $name);
    return $view->render($response, 'register_isusernametaken.html.twig', ['isTaken' => ($service != null) ]);
});

$app->get("/admin/appointments", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);       
    $appointmentsList = DB::query("SELECT a.id AS appointmentId,av.availableDate as appointDate, av.startTime,av.endTime,a.description,a.status,p.petName,p.petType,p.age AS petAge,s.name AS serviceName,d.name AS doctorName
                                    FROM appointments AS a 
                                    join availibilities  as av on a.availibilityId = av.id 
                                    Join pets AS p ON a.petId = p.id
                                    JOIN services AS s ON a.serverId = s.id
                                    JOIN doctors AS d ON a.doctorId = d.id");
    return $view->render($response, 'admin_appointments.html.twig',[ 'appointmentsList'=>$appointmentsList]);
});


$app->get("/admin/doctors", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);       
    $doctorsList = DB::query("SELECT d.id ,d.name,d.license,d.phone,d.email,d.username,d.password,d.photoFilePath AS photoFile ,d.isAdmin,d.description , s.name AS service,c.name AS category 
                                    FROM doctors AS d                                      
                                    JOIN services AS s ON d.serviceId = s.id
                                    JOIN categories AS c ON s.categoryId = c.id                                                           
                                   ORDER BY  d.id");
    return $view->render($response, 'admin_doctors.html.twig',[ 'doctorsList'=>$doctorsList]);
});

$app->get("/admin/pets", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);       
    $petsList = DB::query("SELECT  * FROM pets                                                                    
                                   ORDER BY id");
    return $view->render($response, 'admin_pets.html.twig',[ 'petsList'=>$petsList]);
});

$app->get("/admin/services", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);       
    $servicesList = DB::query("SELECT  s.id, count(d.id) AS totalDoctors ,s.name,s.description,c.name AS categoryName 
                                 FROM services AS s
                                 JOIN categories AS c ON s.categoryId = c.id
                                 JOIN doctors AS d ON d.serviceId = s.id
                                 GROUP BY s.id                                 
                                 ORDER BY id");    
    return $view->render($response, 'admin_services.html.twig',[ 'servicesList'=>$servicesList]);
});

$app->get("/admin/categories", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);     
    $categoriesList = DB::query("SELECT  c.id, count(s.id) AS totalServices ,c.name 
                                FROM categories AS c
                                JOIN services AS s ON s.categoryId = c.id                                
                                GROUP BY c.id                                 
                                ORDER BY c.id");
    return $view->render($response, 'admin_categories.html.twig',[ 'categoriesList'=>$categoriesList]);
});


$app->get("/admin/categories/add", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    return $view->render($response, 'admin_categories_add.html.twig');
});

$app->post("/admin/categories/add", function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    $postVars = $request->getParsedBody();
    $name = $postVars['name'];   
    $errorsList = array();
    
    if (strlen($name) < 2 || strlen($name) > 100) {
        array_push($errorsList, "Name must be 2-100 characters long");
        $postVars ['name'] = "";
    }   
   
    if ($errorsList) { // STATE 2: failed submission
        return $view->render($response, 'admin_categories_add.html.twig', [
            'name'=>$name,'errorsList' => $errorsList
        ]);
    }else{
        DB::insert('categories',['name'=>$name]);
        return $view->render($response, 'category_add_success.html.twig', ['name'=>$name]);
    }
    
});

$app->get('/admin/doctors/edit/{id:[0-9]+}', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);  
    $services =  DB::query("SELECT * FROM services ");       
    $doctorId = $args['id'];   
    $doctor = DB::queryFirstRow("SELECT * FROM doctors WHERE id=:i", $doctorId);
    $selectedDoctorService = DB::queryFirstRow("SELECT s.id,s.name 
                                FROM services AS s
                                JOIN  doctors AS d
                                 On s.id = d.serviceId
                                WHERE d.id=:i", $doctorId);
   if (!$doctor) {
    return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    return $view->render($response, 'admin_doctors_addedit.html.twig',['v'=>$doctor,'selectedDoctorService'=> $selectedDoctorService,'services'=>$services]);
});

$app->post('/admin/doctors/edit/{id:[0-9]+}', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);   
    $doctorId = $args['id']; 
    $services =  DB::query("SELECT * FROM services ");
   $errorsList = array();
   $postVars = $request->getParsedBody();
   $name = $postVars['name'];
   $photoFilePath = $postVars['photo'];
   $serviceId = $postVars['service'];
   $description= $postVars['description'];    
   $address = $postVars['address'];    
   $license = $postVars['license'];  
   $city = $postVars['city'];
   $province = $postVars['province'];
   $country = $postVars['country'];
   $postcode = $postVars['postcode'];
   $phone = $postVars['phone'];
   $email = $postVars['email'];
   $username = $postVars['username'];
   $password = $postVars['password'];       
   $isAdmin = $postVars['isAdmin'];
   
   
   if (strlen($name) < 2 || strlen($name) > 50) {
    array_push($errorsList, "Name must be 2-50 characters long");
    $postVars['name'] = "";
    }
    if (strlen($license) < 2 || strlen($license) > 20) {
    array_push($errorsList, "License must be 2-20 characters long");
    $postVars['license'] = "";
    }
    if (strlen($address) < 2 || strlen($address) > 100) {
    array_push($errorsList, "Address must be 2-100 characters long");
    $postVars['address'] = "";
    }
    if (strlen($city) < 2 || strlen($city) > 15) {
    array_push($errorsList, "City must be 2-15 characters long");
    $postVars['city'] = "";
    }
    if (strlen($province) < 2 || strlen($province) > 10) {
    array_push($errorsList, "Province must be 2-10 characters long");
    $postVars['province'] = "";
    }
    if (strlen($country) < 2 || strlen($country) > 15) {
    array_push($errorsList, "Country must be 2-15 characters long");
    $postVars['country'] = "";
    }
    if (strlen($postcode) < 2 || strlen($postcode) > 10) {
    array_push($errorsList, "Postcode must be 2-10 characters long");
    $postVars['postcode'] = "";
    }
    if (strlen($phone) < 2 || strlen($phone) > 24) {
    array_push($errorsList, "Phone must be 2-24 characters long");
    $postVars['phone'] = "";
    }
    if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE ){
    array_push($errorsArray,"Error: Email does not look valid");
    $postVars ['email'] = "";
    }      
    if (strlen($username) < 2 || strlen($username) > 30) {
    array_push($errorsList, "Username must be 2-30 characters long");
    $postVars['username'] = "";
    }
    if ((strlen($password) < 6)
            || (preg_match("/[A-Z]/", $password) == FALSE )
            || (preg_match("/[a-z]/", $password) == FALSE )
            || (preg_match("/[0-9]/", $password) == FALSE )) {
    array_push($errorsList, "Password must be at least 6 characters long, "
            . "with at least one uppercase, one lowercase, and one digit in it");
    }
    if (strlen($description) < 2 || strlen($description) > 5000) {
    array_push($errorsList, "Description must be 2-10000 characters long");
    $postVars['description'] = "";
    }    
    if($errorsList){
        return $view->render($response, 'admin_doctors_addedit.html.twig', ['v'=>$postVars,'errorsList' => $errorsList]);
    }else{
        DB::update('doctors',['name'=>$name,'serviceId'=>$serviceId,'address'=>$address,'city'=>$city,'province'=>$province,'country'=>$country,'postcode'=>$postcode,
        'phone'=>$phone,'email'=>$email,'username'=>$username,'password'=>$password, 'isAdmin'=>$isAdmin, 'description'=>$description, 'license'=>$license,'photoFilePath'=>$photoFilePath],"id=:i", $doctorId);
        $doctorsList = DB::query("SELECT d.id ,d.name,d.license,d.phone,d.email,d.username,d.password,d.photoFilePath AS photoFile ,d.isAdmin,d.description , s.name AS service,c.name AS category 
                                 FROM doctors AS d                                      
                                JOIN services AS s ON d.serviceId = s.id
                                JOIN categories AS c ON s.categoryId = c.id                                                           
                                ORDER BY  d.id");
        return $view->render($response, 'admin_doctors.html.twig',[ 'doctorsList'=>$doctorsList]);
    }      
});

$app->get('/admin/doctors/add', function(Request $request, Response $response, array $args){
     $view = Twig::fromRequest($request);         
    $services =  DB::query("SELECT * FROM services ");
        return $view->render($response, 'admin_doctors_addedit.html.twig',['services'=>$services]);
});

$app->post('/admin/doctors/add', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);   
    $services =  DB::query("SELECT * FROM services ");
   $errorsList = array();
   $postVars = $request->getParsedBody();
   $name = $postVars['name'];
   $serviceId = $postVars['service'];
   $license = $postVars['license'];
   $address = $postVars['address'];
   $city = $postVars['city'];
   $province = $postVars['province'];
   $country = $postVars['country'];
   $postcode = $postVars['postcode'];
   $phone = $postVars['phone'];
   $email = $postVars['email'];
   $username = $postVars['username'];
   $password = $postVars['password'];
   $description= $postVars['description'];       
   $isAdmin = $postVars['isAdmin'];

   //handle the image file  that has been choosen in twig
    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();       
    $uploadedFile = $uploadedFiles['photoFilePath'];    
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filename = moveUploadedFile($directory, $uploadedFile);       
    } else {
        array_push($errorsList, "upload failed!"); 
    }    
   
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorsList, "Name must be 2-50 characters long");
        $postVars['name'] = "";
    }
    if (strlen($license) < 2 || strlen($license) > 20) {
        array_push($errorsList, "License must be 2-20 characters long");
        $postVars['license'] = "";
    }
    if (strlen($address) < 2 || strlen($address) > 100) {
        array_push($errorsList, "Address must be 2-100 characters long");
        $postVars['address'] = "";
    }
    if (strlen($city) < 2 || strlen($city) > 15) {
        array_push($errorsList, "City must be 2-15 characters long");
        $postVars['city'] = "";
    }
    if (strlen($province) < 2 || strlen($province) > 10) {
        array_push($errorsList, "Province must be 2-10 characters long");
        $postVars['province'] = "";
    }
    if (strlen($country) < 2 || strlen($country) > 15) {
        array_push($errorsList, "Country must be 2-15 characters long");
        $postVars['country'] = "";
    }
    if (strlen($postcode) < 2 || strlen($postcode) > 10) {
        array_push($errorsList, "Postcode must be 2-10 characters long");
        $postVars['postcode'] = "";
    }
    if (strlen($phone) < 2 || strlen($phone) > 24) {
        array_push($errorsList, "Phone must be 2-24 characters long");
        $postVars['phone'] = "";
    }
    if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE ){
        array_push($errorsArray,"Error: Email does not look valid");
        $postVars ['email'] = "";
    }      
    if (strlen($username) < 2 || strlen($username) > 30) {
        array_push($errorsList, "Username must be 2-30 characters long");
        $postVars['username'] = "";
    }
    if ((strlen($password) < 6)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
        array_push($errorsList, "Password must be at least 6 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
    }
    if (strlen($description) < 2 || strlen($description) > 5000) {
        array_push($errorsList, "Description must be 2-10000 characters long");
        $postVars['description'] = "";
    }    
    if($errorsList){
        return $view->render($response, 'admin_doctors_addedit.html.twig', ['v'=>$postVars,'errorsList' => $errorsList]);
    }else{
        DB::insert('doctors',['name'=>$name,'serviceId'=>$serviceId,'address'=>$address,'city'=>$city,'province'=>$province,'country'=>$country,'postcode'=>$postcode,
          'phone'=>$phone,'email'=>$email,'username'=>$username,'password'=>$password, 'isAdmin'=>$isAdmin, 'description'=>$description, 'license'=>$license,'photoFilePath'=>'/images/staff/'.$filename]);
          $doctorsList = DB::query("SELECT d.id ,d.name,d.license,d.phone,d.email,d.username,d.password,d.photoFilePath AS photoFile ,d.isAdmin,d.description , s.name AS service,c.name AS category 
                                     FROM doctors AS d                                      
                                    JOIN services AS s ON d.serviceId = s.id
                                    JOIN categories AS c ON s.categoryId = c.id                                                           
                                    ORDER BY  d.id");
        return $view->render($response, 'admin_doctors.html.twig',[ 'doctorsList'=>$doctorsList]);
    }      
});

function moveUploadedFile($directory, UploadedFileInterface $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    return $filename;
}

$app->get('/admin/doctors/delete/{id:[0-9]+}', function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);    
    $doctorId = $args['id'];    
    DB::delete("doctors", "id=:i", $doctorId);   
    $doctorsList = DB::query("SELECT d.id ,d.name,d.license,d.phone,d.email,d.username,d.password,d.photoFilePath AS photoFile ,d.isAdmin,d.description , s.name AS service,c.name AS category 
                                FROM doctors AS d                                      
                                JOIN services AS s ON d.serviceId = s.id
                                JOIN categories AS c ON s.categoryId = c.id                                                           
                                ORDER BY  d.id");
    return $view->render($response, 'admin_doctors.html.twig',[ 'doctorsList'=>$doctorsList]);      
});

$app->get('/admin/services/edit/{id:[0-9]+}', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);  
    $categories =  DB::query("SELECT * FROM categories ");    
    $serviceId = $args['id'];  
    $service = DB::queryFirstRow("SELECT * FROM services WHERE id=:i", $serviceId);   
    $selectedServiceCategory = DB::queryFirstRow("SELECT c.id,c.name 
                                FROM categories AS c
                                JOIN  services AS s
                                 On c.id = s.categoryId
                                WHERE s.id=:i", $serviceId);
  
    return $view->render($response, 'admin_services_addedit.html.twig',['v'=>$service,'selectedServiceCategory'=> $selectedServiceCategory,'categories'=>$categories]);
});

$app->post('/admin/services/edit/{id:[0-9]+}', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);   
    $categories =  DB::query("SELECT * FROM categories ");    
    $serviceId = $args['id'];  
   $errorsList = array();
   $postVars = $request->getParsedBody();
   $name = $postVars['name'];
   $categoryId = $postVars['category'];   
   $description= $postVars['description'];       
   
   //handle the image file  that has been choosen in twig    
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorsList, "Name must be 2-50 characters long");
        $postVars['name'] = "";
    }    
    if (strlen($description) < 2 || strlen($description) > 5000) {
        array_push($errorsList, "Description must be 2-10000 characters long");
        $postVars['description'] = "";
    }    
    if($errorsList){
        return $view->render($response, 'admin_doctors_addedit.html.twig', ['v'=>$postVars,'errorsList' => $errorsList]);
    }else{
        DB::update('services',['name'=>$name,'categoryId'=>$categoryId,'description'=>$description],"id=:i", $serviceId);
        $servicesList = DB::query("SELECT  s.id, count(d.id) AS totalDoctors ,s.name,s.description,c.name AS categoryName 
                                    FROM services AS s
                                    JOIN categories AS c ON s.categoryId = c.id
                                    JOIN doctors AS d ON d.serviceId = s.id
                                    GROUP BY s.id                                 
                                    ORDER BY id");        
        return $view->render($response, 'admin_services.html.twig',[ 'servicesList'=>$servicesList]);
    }      
});

$app->get('/admin/services/add', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);   
    $categories =  DB::query("SELECT * FROM categories ");
    return $view->render($response, 'admin_services_addedit.html.twig',['categories'=>$categories]);
});

$app->post('/admin/services/add', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);   
    $categories =  DB::query("SELECT * FROM categories ");
   $errorsList = array();
   $postVars = $request->getParsedBody();
   $name = $postVars['name'];
   $categoryId = $postVars['category'];   
   $description= $postVars['description'];       
   
   //handle the image file  that has been choosen in twig    
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorsList, "Name must be 2-50 characters long");
        $postVars['name'] = "";
    }    
    if (strlen($description) < 2 || strlen($description) > 5000) {
        array_push($errorsList, "Description must be 2-10000 characters long");
        $postVars['description'] = "";
    }    
    if($errorsList){
        return $view->render($response, 'admin_services_addedit.html.twig', ['v'=>$postVars,'categories'=>$categories,'errorsList' => $errorsList]);
    }else{
        DB::insert('services',['name'=>$name,'categoryId'=>$categoryId,'description'=>$description]);
        $servicesList = DB::query("SELECT  s.id, count(d.id) AS totalDoctors ,s.name,s.description,c.name AS categoryName 
                                    FROM services AS s
                                    JOIN categories AS c ON s.categoryId = c.id
                                    JOIN doctors AS d ON d.serviceId = s.id
                                    GROUP BY s.id                                 
                                    ORDER BY id");        
        return $view->render($response, 'admin_services.html.twig',[ 'servicesList'=>$servicesList]);
    }      
});

$app->get('/admin/services/delete/{id:[0-9]+}', function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);    
    $serviceId = $args['id'];    
    DB::delete("services", "id=:i", $serviceId);   
    $servicesList = DB::query("SELECT  s.id, count(d.id) AS totalDoctors ,s.name,s.description,c.name AS categoryName 
                                 FROM services AS s
                                 JOIN categories AS c ON s.categoryId = c.id
                                 JOIN doctors AS d ON d.serviceId = s.id
                                 GROUP BY s.id                                 
                                 ORDER BY id");   
    return $view->render($response, 'admin_services.html.twig',[ 'servicesList'=>$servicesList,]);   
});

$app->get('/admin/pets/add', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);    
    return $view->render($response, 'admin_pets_addedit.html.twig');
});

$app->post('/admin/pets/add', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);       
   $errorsList = array();
   $postVars = $request->getParsedBody();
   $petName = $postVars['petName'];
   $petType = $postVars['petType'];
   $petAge = $postVars['petAge'];   
   $ownerName = $postVars['ownerName'];
   $ownerPhone = $postVars['ownerPhone'];
   $ownerEmail = $postVars['ownerEmail'];
   $username = $postVars['username'];   
   $password = $postVars['password'];   

   //handle the image file  that has been choosen in twig   
    if (strlen($petName) < 2 || strlen($petName) > 100) {
        array_push($errorsList, "Pet name must be 2-100 characters long");
        $postVars['petName'] = "";
    }
    if (strlen($petType) < 2 || strlen($petType) > 20) {
        array_push($errorsList, "Pet type must be 2-20 characters long");
        $postVars['petType'] = "";
    }
    if ($petAge < 1 || $petAge > 25) {
        array_push($errorsList, "Pet age must be 1-25 .");
        $postVars['petAge'] = "";
    }
    if (strlen($ownerName) < 2 || strlen($ownerName) > 100) {
        array_push($errorsList, "Owner name must be 2-100 characters long");
        $postVars['ownerName'] = "";
    }
    if (strlen($ownerPhone) < 2 || strlen($ownerPhone) > 24) {
        array_push($errorsList, "Owner phone must be 2-24 characters long");
        $postVars['ownerPhone'] = "";
    }    
    if(filter_var($ownerEmail, FILTER_VALIDATE_EMAIL) == FALSE ){
        array_push($errorsArray,"Error: Email does not look valid");
        $postVars ['ownerEmail'] = "";
    }      
    if (strlen($username) < 2 || strlen($username) > 30) {
        array_push($errorsList, "Username must be 2-30 characters long");
        $postVars['username'] = "";
    }
    if ((strlen($password) < 6)
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
        array_push($errorsList, "Password must be at least 6 characters long, "
                . "with at least one uppercase, one lowercase, and one digit in it");
    }
   
    if($errorsList){
        return $view->render($response, 'admin_pets_addedit.html.twig', ['v'=>$postVars,'errorsList' => $errorsList]);
    }else{
        DB::insert('pets',['petName'=>$petName,'petType'=>$petType,'age'=>$petAge,'ownerName'=>$ownerName,'ownerPhone'=>$ownerPhone,'ownerEmail'=>$ownerEmail,'username'=>$username,'password'=>$password]);
        $petsList = DB::query("SELECT  * FROM pets                                                                 
                                ORDER BY id");
        return $view->render($response, 'admin_pets.html.twig',[ 'petsList'=>$petsList]);
    }      
});

$app->get('/admin/pets/edit/{id:[0-9]+}', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);             
    $petId = $args['id'];   
    $pet = DB::queryFirstRow("SELECT * FROM pets WHERE id=:i", $petId);    
   if (!$pet) {
    return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
    }
    return $view->render($response, 'admin_pets_addedit.html.twig',['v'=>$pet]);
});

$app->post('/admin/pets/edit/{id:[0-9]+}', function(Request $request, Response $response, array $args){
    $view = Twig::fromRequest($request);   
    $petId = $args['id'];      
   $errorsList = array();
   $postVars = $request->getParsedBody();
   $petName = $postVars['petName'];
   $petType = $postVars['petType'];
   $petAge = $postVars['petAge'];   
   $ownerName = $postVars['ownerName'];
   $ownerPhone = $postVars['ownerPhone'];
   $ownerEmail = $postVars['ownerEmail'];
   $username = $postVars['username'];   
   $password = $postVars['password'];
   
   
   if (strlen($petName) < 2 || strlen($petName) > 100) {
    array_push($errorsList, "Pet name must be 2-100 characters long");
    $postVars['petName'] = "";
    }
    if (strlen($petType) < 2 || strlen($petType) > 20) {
    array_push($errorsList, "Pet type must be 2-20 characters long");
    $postVars['petType'] = "";
    }
    if ($petAge < 1 || $petAge > 25) {
    array_push($errorsList, "Pet age must be 1-25 .");
    $postVars['petAge'] = "";
    }
    if (strlen($ownerName) < 2 || strlen($ownerName) > 100) {
    array_push($errorsList, "Owner name must be 2-100 characters long");
    $postVars['ownerName'] = "";
    }
    if (strlen($ownerPhone) < 2 || strlen($ownerPhone) > 24) {
    array_push($errorsList, "Owner phone must be 2-24 characters long");
    $postVars['ownerPhone'] = "";
    }    
    if(filter_var($ownerEmail, FILTER_VALIDATE_EMAIL) == FALSE ){
    array_push($errorsArray,"Error: Email does not look valid");
    $postVars ['ownerEmail'] = "";
    }      
    if (strlen($username) < 2 || strlen($username) > 30) {
    array_push($errorsList, "Username must be 2-30 characters long");
    $postVars['username'] = "";
    }
    if ((strlen($password) < 6)
            || (preg_match("/[A-Z]/", $password) == FALSE )
            || (preg_match("/[a-z]/", $password) == FALSE )
            || (preg_match("/[0-9]/", $password) == FALSE )) {
    array_push($errorsList, "Password must be at least 6 characters long, "
            . "with at least one uppercase, one lowercase, and one digit in it");
    }
    if($errorsList){
        return $view->render($response, 'admin_pets_addedit.html.twig', ['v'=>$postVars,'errorsList' => $errorsList]);
    }else{
        DB::update('pets',['petName'=>$petName,'petType'=>$petType,'age'=>$petAge,'ownerName'=>$ownerName,'ownerPhone'=>$ownerPhone,
        'ownerEmail'=>$ownerEmail,'username'=>$username,'password'=>$password],"id=:i", $petId);
        $petsList = DB::query("SELECT  * FROM pets                                                                 
                                ORDER BY id");
        return $view->render($response, 'admin_pets.html.twig',[ 'petsList'=>$petsList]);
    }      
});

$app->get('/admin/pets/delete/{id:[0-9]+}', function(Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);    
    $petId = $args['id'];    
    DB::delete("pets", "id=:i", $petId);   
    $petsList = DB::query("SELECT  * FROM pets              
                         ORDER BY id");
    return $view->render($response, 'admin_pets.html.twig',[ 'petsList'=>$petsList]);    
});




